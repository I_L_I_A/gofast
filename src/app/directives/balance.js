(function() {
  'use strict';

  angular
    .module('app')
    .directive('gfBalance', RoundDirective);

  function RoundDirective() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/views/balance/balance.html',
      controller: RoundController,
      controllerAs: 'bl',
      bindToController: true
    };

    return directive;

    function RoundController($scope){
      $scope.round = {
      allMoney: 999999998,
      percent: 999999999,
      dayRemaining: moment().endOf('month').diff(moment().today, 'days')
    };

    $scope.strFilter = function(number) {
          var price = new String(number);
          if (price.length > 3) {
              var length = price.length;
              var temp = [];
              for (var i = 0; i < length; i++) {
                  temp.push(price[i]);
                  if ((length - 1 - i) % 3 == 0) {
                      temp.push(' ');
                  }
              }
              price = temp.join('');
          }

          return price;
      }

    }
  }
})();
