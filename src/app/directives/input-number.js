angular.module('app').directive("inputNumber", ['$parse', '$compile', function ($parse, $compile) {
    return {
        restrict: 'A',
        priority: 1001,
        require: 'ngModel',
        scope: true,
        compile: function (el, attrs) {
            return function ($scope, $elem, $attrs, $modelCtrl) {
                $scope.validate = function (modelValue) {

                    modelValue = parseInt(modelValue);
                    var isInteger = new RegExp(/^\d+$/).test(model);
                    if (modelValue) {
                        if (modelValue > $scope.maxValue) {
                            $scope.model.assign($scope, $scope.maxValue);
                        }
                        if (modelValue < $scope.minValue) {
                            $scope.model.assign($scope, "");
                        }
                    } else {
                        $scope.model.assign($scope, "");
                    }
                };
                $elem.replaceWith($compile($elem.clone()
                                    .removeAttr("input-number")
                                    .attr("ng-change", "validate(" + $attrs.ngModel + ")"))($scope));
                $scope.minValue = $attrs.min ? parseInt($attrs.min) : 0;
                $scope.maxValue = $attrs.max? parseInt($attrs.max) : 999;
                $scope.model = $parse($attrs.ngModel);
            };
        }
    };
}]);
angular.module('app').directive('nksOnlyNumber', function () {
      return {
          restrict: 'EA',
          require: 'ngModel',
          link: function (scope, element, attrs, ngModel) {   
               scope.$watch(attrs.ngModel, function(newValue, oldValue) {
                    var spiltArray = String(newValue).split("");

                    if(attrs.allowNegative == "false") {
                      if(spiltArray[0] == '-') {
                        newValue = newValue.replace("-", "");
                        ngModel.$setViewValue(newValue);
                        ngModel.$render();
                      }
                    }

                    if(attrs.allowDecimal == "false") {
                        newValue = parseInt(newValue);
                        ngModel.$setViewValue(newValue);
                        ngModel.$render();
                    }

                    if(attrs.allowDecimal != "false") {
                      if(attrs.decimalUpto) {
                         var n = String(newValue).split(".");
                         if(n[1]) {
                            var n2 = n[1].slice(0, attrs.decimalUpto);
                            newValue = [n[0], n2].join(".");
                            ngModel.$setViewValue(newValue);
                            ngModel.$render();
                         }
                      }
                    }


                    if (spiltArray.length === 0) return;
                    if (spiltArray.length === 1 && (spiltArray[0] == '-' || spiltArray[0] === '.' )) return;
                    if (spiltArray.length === 2 && newValue === '-.') return;

                  /*Check it is number or not.*/
                  if (isNaN(newValue)) {
                    ngModel.$setViewValue(oldValue);
                    ngModel.$render();
                  }
              });
          }
      };
  });