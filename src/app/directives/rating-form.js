(function() {
  'use strict';

  angular
    .module('app')
    .directive('gfRatingForm', RatingFormDirective);

  function RatingFormDirective() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/views/rating/ratingForm.html',
      controller: RatingFormController,
      controllerAs: 'rs',
      bindToController: true
    };

    return directive;

    /** @ngInject */
    function RatingFormController($scope) {
      $scope.valuationData = {
        number: 232345,
        title: '',
        comment: '',
        stars: [
          {
            name:'Надёжность/Комфорт',
            ratingValue: 0,
            value: 0
          },
          {
            name:'Ответственность',
            ratingValue: 0,
            value: 0
          },
          {
            name:'Пунктуальность',
            ratingValue: 0,
            value: 0
          },
          {
            name:'Вежливость',
            ratingValue: 0,
            value: 0
          }
        ]
      };

      $scope.safety = true;
      $scope.comfort = true;


      //set watchers of the value changing

      for(var i=0; i<$scope.valuationData.stars.length; i++) {
        $scope.$watch('valuationData.stars['+i+']', function(changed) {
            changed.ratingValue = 20 * changed.value;
        }, true);
      }


      //stars data

      $scope.rate = 3;
      $scope.max = 5;
      $scope.isReadonly = false;

      $scope.hoveringOver = function(value) {
        var self = this;
        self.overStar = value/10;
        self.percent = 100 * (value / $scope.max);
      };


      // for sending

      $scope.feedback = {
        number: 0,
        title: '',
        comment:'',
        rating:{
          safety: 0,
          responsibility: 0,
          punctuality: 0,
          politeness: 0
        }
      };

      // button click

      $scope.sendComment = function(stars){
        $scope.feedback.number = $scope.valuationData.number;
        $scope.feedback.title = $scope.valuationData.title;
        $scope.feedback.comment = $scope.valuationData.comment;

        $scope.feedback.rating.safety = stars[0].ratingValue;
        $scope.feedback.rating.responsibility = stars[1].ratingValue;
        $scope.feedback.rating.punctuality = stars[2].ratingValue;
        $scope.feedback.rating.politeness = stars[3].ratingValue;
      }
    }
}

})();
