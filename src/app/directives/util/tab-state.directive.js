angular.module('app')
  .directive('tabState', ['$state', function($state) {
    return {
      restrict: 'A',
      link: function (scope, element, attrs) {
        scope.$watch(function() {
          return $state.current.name;
        }, function(stateName) {
           if(stateName === attrs.uiSref) {
             element.addClass('_active');
           }
          else {
            element.removeClass('_active');
          }
        });
      }
    };
  }]);