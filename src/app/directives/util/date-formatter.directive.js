/** 
* Prevent default action on empty links.
*/
angular.module('app')
  .directive('dateFormatter', ['$filter', function($filter) {
    return {
      require: 'ngModel',
      link: function(scope, element, attrs, controller) {
       controller.$formatters.push(function(value) {
          return $filter('date')(value, "dd/MM/yy hh:mm");
        });
      }
    };
  }]);
