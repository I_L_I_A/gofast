/** 
* Prevent default action on empty links.
*/
angular.module('app')
  .directive('a', ['$state', function($state) {
   return {
        restrict: 'E',
        link: function (scope, elem, attrs) {
            if (attrs.ngClick || attrs.href === '' || attrs.href === '#') {
                elem.on('click', function (e) {
                  //  e.preventDefault();
                });
            }
        }
    };
  }]);
