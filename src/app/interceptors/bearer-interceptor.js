(function () {

  'use strict';

  function bearerInterceptor($q, $injector, $location, localStorageService, API, AuthService, OAUTH) {
  
    var updatingRefreshToken = false;
    var refreshTokenDeferred = null;

    function request(config) {
      config.headers = config.headers || {};
      var authData = localStorageService.get('authorization_data');
      if (authData) {
          config.headers.Authorization = 'Bearer ' + authData.token;
      }
      config.requestTimestamp = new Date().getTime();
      return config;
    }

    function responseError(rejection) {
      var deferred = $q.defer();

      if (rejection.status === 401) {
        var authorizationData = localStorageService.get('authorization_data');
        if (authorizationData.refreshTokenTimestamp > rejection.config.requestTimestamp) {
            retryHttpRequest(rejection.config, deferred);
        } else {
          updateRefreshToken().then(function (response) {
            retryHttpRequest(rejection.config, deferred);
          }, function () {
            AuthService.logOut();
            $location.path('/login');
            deferred.reject(rejection);
          });
        }
      } else {
        deferred.reject(rejection);
      }
      
      return deferred.promise;
    }
  
    function retryHttpRequest(config, deferred) {
      $injector.get('$http')(config).then(function (response) {
          deferred.resolve(response);
      }, function (response) {
          deferred.reject(response);
      });
    }
  
    function updateRefreshToken() {
      if (!updatingRefreshToken) {
        updatingRefreshToken = true;
        refreshTokenDeferred = $q.defer();
        var authData = localStorageService.get('authorization_data');
        var data = 'grant_type=' + OAUTH.REFRESH_TOKEN + '&refresh_token=' + authData.refreshToken + '&client_id=' + OAUTH.ID + '&client_secret=test' + OAUTH.SECRET;
        $injector.get('$http').post(API.TOKEN, data, {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        })
        .success(function (response) {
          localStorageService.set('authorization_data', {
            token: response.access_token,
            userName: response.userName,
            userRole: response.user_role,
            refreshToken: response.refresh_token,
            refreshTokenTimestamp: new Date().getTime()
          });
          updatingRefreshToken = false;
          refreshTokenDeferred.resolve(response);
        })
        .error(function (error) {
          updatingRefreshToken = false;
          refreshTokenDeferred.reject(error);
        });
      }
      return refreshTokenDeferred.promise;
    }

    return {
      request: request,
      responseError: responseError
    };

  }
    
  angular.module('app').factory('bearerInterceptor', ['$q', '$injector', '$location', 'localStorageService', 'API', 'AuthService', 'OAUTH', bearerInterceptor]);
})();