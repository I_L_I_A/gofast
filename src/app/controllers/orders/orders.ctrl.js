(function () {

	'use strict';

  function OrdersCtrl($scope, $state, DataService, $filter) {
		$scope.orders = [];
    var _map;
    $scope.mapCenter = [27.561524, 53.904540];
    $scope.reverse = false;
    $scope.toggleMap = true;
		getOrders();

    $scope.afterMapInit = function(map){
      _map = map;
      drawRoutes($scope.orders);
    };

		$scope.sortBy = function(predicate, reverse) {
      $scope.reverse = reverse || !$scope.reverse;
      $scope.orders = $filter('orderBy')($scope.orders, predicate, $scope.reverse);
    };

  	function getOrders(params) {
  		DataService.getOrdersList()
  			.success(function(data){
					$scope.orders = formatData(data.result);
					calculatePaging(data);
  			})
  	}

  	function formatData (data) {
  		angular.forEach(data, function(order){
  			var currentDate = moment(new Date());
        if(order.route.departureTime.length > 1){
          order.activeFor = currentDate.to(order.route.departureTime[1]);
        } else {
          order.activeFor = currentDate.to(order.route.departureTime[0]);
        }
  			
        order.departureTime = order.route.departureTime[0];
        order.arrivalTime = order.route.arrivalTime[0];
        order.routeLength = order.route.length;
  		})

  		return data;
  	}

  	function calculatePaging(data) {
  		$scope.totalPages = data.totalPages;
  		$scope.totalItems = data.totalItems;
  		$scope.currentPage = data.page;
  	}

    function drawRoutes(orders){
      angular.forEach(orders, function(order){
        var from = order.route.from;
        var to = order.route.to;

        var longFrom= from.coordinate.long;
        var latFrom = from.coordinate.lat;
        var longTo = to.coordinate.long;
        var latTo = to.coordinate.lat;

        var cityFrom = from.address.city;
        var cityTo = to.address.city;
        var addressFrom = from.address.street + ', ' + from.address.house + '-' + from.address.flat;
        var addressTo = to.address.street + ', ' + to.address.house + '-' + to.address.flat;

        var routePoints = [ [longFrom, latFrom], [longTo, latTo] ];

        ymaps.route(routePoints)
          .then(function (route){
            route.getPaths().options.set({
              strokeColor: '#3e92e6',
              opacity: 0.7
            });
            var points = route.getWayPoints();

            // https://tech.yandex.ru/maps/doc/jsapi/2.0/dg/concepts/geoobjects-docpage/
            // http://catatron.com/angular-ymaps/
            var pointFrom = points.get(0);
            pointFrom.options.set('iconColor', 'green'); // rgb also can be

            var pointFromProperties = pointFrom.properties;
            pointFromProperties.set('iconContent', '');
            pointFromProperties.set('iconColor', '#3caa3c');
            pointFromProperties.set('hintContent', 'Выезд');
            pointFromProperties.set('balloonContentHeader', cityFrom);
            pointFromProperties.set('balloonContentBody', addressFrom);

            var pointTo = points.get(1);
            pointTo.options.set('iconColor', 'red'); // rgb also can be

            var pointToProperties = points.get(1).properties;
            pointToProperties.set('iconContent', '');
            pointToProperties.set('iconColor', '#3caa3c');
            pointToProperties.set('hintContent', 'Приезд');
            pointToProperties.set('balloonContentHeader', cityTo);
            pointToProperties.set('balloonContentBody', addressTo);

            _map.geoObjects.add(route);
          });
      });
    }

  }
    
  angular.module('app').controller('orders.ctrl', ['$scope', '$state', 'DataService', '$filter', OrdersCtrl]);
})();