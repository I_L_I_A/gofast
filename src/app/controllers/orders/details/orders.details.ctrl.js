(function () {

	'use strict';

  function OrdersDetailsCtrl($scope, $state, $stateParams, $modal, $filter, YEARS, MONTHS, PHONE_CODES, OrderService) {
    $scope.toggleMap = true;
    $scope.reverse = false;
    if(!$stateParams.orderId){
  		$state.go("app.orders.list");
  	}
		$scope.getBackToOrdersList = function(){
			//TODO: think of the following case: you search for an order in app.orders.list, specify some search params, go to the order details and 
			//then hit "к списку заказов" link. It would be cool to see the same search params there, isn't it? 
			//Need to think how to restore that state.
			$state.go("app.orders.list");
		};
		OrderService.get($stateParams.orderId).then(function(response){
			$scope.order = response.data;
			var currentDate = moment(new Date());

      if($scope.order.route.departureTime.length > 1)
      {
        var departureDate = moment($scope.order.route.departureTime[1]);
        $scope.order.activeFor = new Date() < $scope.order.route.departureTime[1]? currentDate.to(departureDate) : "Завершена";
      } else {
        var departureDate = moment($scope.order.route.departureTime[0]);
        $scope.order.activeFor = new Date() < $scope.order.route.departureTime[0]? currentDate.to(departureDate) : "Завершена";
      }
		});

		//TODO: refactor order service to consume additional params, e.g "sort" property and "reverse"
		OrderService.getOrderBids({orderId: $stateParams.orderId}).then(function(response){
			$scope.orderBids = response.data;
			$scope.minimalBid = 0
			if($scope.orderBids.length > 0){
				$scope.minimalBid = $filter('orderBy')($scope.orderBids, 'price', false)[0].price;
			}
		});

		$scope.sortBidsBy = function(predicate, reverse) {
      $scope.reverse = reverse || !$scope.reverse;
      $scope.orderBids = $filter('orderBy')($scope.orderBids, predicate, $scope.reverse);
    };

    $scope.acceptBid = function(){
      OrderService.acceptBid().then(function(response){
        $state.transitionTo($state.current, $stateParams, {
            reload: true,
            inherit: false,
            notify: true
        });
        $modal.open({
          templateUrl: 'app/views/contacts/carrierChosen.html',
          controller: 'carrierChosen.ctrl',
          resolve: {
            carrier: function () {
              return response.data;
            }
          }
        });
      });
    }

  	var _map;
		$scope.drawRoute = function(map){
			_map = map;
      var from = $scope.order.route.from;
      var to = $scope.order.route.to;

      var longFrom= from.coordinate.long;
      var latFrom = from.coordinate.lat;
      var longTo = to.coordinate.long;
      var latTo = to.coordinate.lat;

      var cityFrom = from.address.city;
      var cityTo = to.address.city;
      var addressFrom = from.address.street + ', ' + from.address.house + '-' + from.address.flat;
      var addressTo = to.address.street + ', ' + to.address.house + '-' + to.address.flat;

      var routePoints = [ [longFrom, latFrom], [longTo, latTo] ];

      ymaps.route(routePoints, { mapStateAutoApply: true })
        .then(function (route){
          route.getPaths().options.set({
            strokeColor: '#3e92e6',
            opacity: 0.7
          });
          var points = route.getWayPoints();

          var pointFrom = points.get(0);
          pointFrom.options.set('iconColor', 'green'); // rgb also can be

          var pointFromProperties = pointFrom.properties;
          pointFromProperties.set('iconContent', '');
          pointFromProperties.set('hintContent', 'Начало');
          pointFromProperties.set('balloonContentHeader', cityFrom);
          pointFromProperties.set('balloonContentBody', addressFrom);

          var pointTo = points.get(1);
          pointTo.options.set('iconColor', 'red'); // rgb also can be

          var pointToProperties = pointTo.properties;
          pointToProperties.set('iconContent', '');
          pointToProperties.set('hintContent', 'Конец');
          pointToProperties.set('balloonContentHeader', cityTo);
          pointToProperties.set('balloonContentBody', addressTo);

          _map.geoObjects.add(route);
        });
		}
  }
    
  angular.module('app').controller('orders.details.ctrl', ['$scope', '$state', '$stateParams', '$modal', '$filter', 'YEARS', 'MONTHS', 'PHONE_CODES', 'OrderService', OrdersDetailsCtrl]);
})();