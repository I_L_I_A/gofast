(function () {

	'use strict';

  function OrdersBidCtrl($scope, $state, $stateParams, BidService, YEARS, MONTHS, PHONE_CODES) {
		$scope.orderId = $stateParams.orderId;
		$scope.editMode = $stateParams.editMode;
		$scope.bid = {
		}
		if(!$stateParams.editMode && $stateParams.order){
			$scope.bid = {
				price: $stateParams.order.price.minimalBid,
				pickUpDate: $stateParams.order.route.departureTime[0],
				dropDate: $stateParams.order.route.arrivalTime[0],
				packing: $stateParams.order.needsPacking,
				loading: $stateParams.order.needsLoading,
				unloading: $stateParams.order.needsUnloading,
			}
		}

		if($stateParams.editMode && $stateParams.bid){
			$scope.bid = $stateParams.bid;
		}

		$scope.submitBid = function(){
			if($stateParams.editMode){
				BidService.updateBid($scope.bid);
			} else {
				BidService.addBid($scope.bid);
			}
			$state.go("app.orders.details", {orderId: $scope.orderId});
		}
  };
    
  angular.module('app').controller('orders.bid.ctrl', ['$scope', '$state', '$stateParams', 'BidService', 'YEARS', 'MONTHS', 'PHONE_CODES', OrdersBidCtrl]);
})();