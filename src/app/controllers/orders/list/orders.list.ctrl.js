(function () {

	'use strict';

  function OrdersListCtrl($scope, $state, $stateParams, CountryCityStreetService, OrderService, YEARS, MONTHS, PHONE_CODES, CategoryService) {
 	  $scope.datepickerOptions = {
	  	maxDate: new Date()
	  };
	  $scope.searchQuery = $stateParams.searchObj;
	  CategoryService.getAll().then(function(categories) {
		  $scope.categories = categories;
	  });
	  CategoryService.getNonPassengerCategories().then(function(categories) {
		  $scope.nonPassengerCategories = categories;
	  });

	  $scope.searchCountries = function(search){
	  	CountryCityStreetService.getManyCountries(search).then(function(countries){
	  		$scope.countries = countries.data;
	  	});
	  };

	  $scope.searchCities = function(search){
	  	CountryCityStreetService.getManyCities(search).then(function(cities){
	  		$scope.cities = cities.data;
	  	});
	  };

	  $scope.searchOrders = function(){
	  	OrderService.getMany(searchQuery);
	  }
  };
    
  angular.module('app').controller('orders.list.ctrl', ['$scope', '$state', '$stateParams', 'CountryCityStreetService', 'OrderService', 'YEARS', 'MONTHS', 'PHONE_CODES', 'CategoryService', OrdersListCtrl]);
})();