(function () {

	'use strict';

	function OrdersCreateShipCtrl($scope, $state, $stateParams, DataService, CategoryService, CountryCityStreetService, $filter) {
		init();

    $scope.searchCountries = function(search){
      CountryCityStreetService.getManyCountries(search).then(function(response){
        $scope.countries = response.data;
      });
    }

    $scope.searchCities = function(countryId, search){
      CountryCityStreetService.getManyCities(countryId, search).then(function(response){
        $scope.cities = response.data;
      });
    }

    $scope.searchStreets = function(countryId, cityId, search){
      CountryCityStreetService.getManyStreets(countryId, cityId, search).then(function(response){
        $scope.streets = response.data;
      });
    }

		$scope.setStep = function (step, $event, category) {
      $scope.item.category = category ? category.name : $scope.item.category;
			$scope.item.categoryId = category ? category.id : $scope.item.categoryId;
			$event.preventDefault();
			$event.stopPropagation();
			$scope.step = step;
      if(step == 2){
        getTags($scope.item.categoryId);
      }
		};

    $scope.onDepartTime1Set = function(newValue, oldValue){
      $scope.item.route.parts[0].departureTime[0] = formatDateTime(newValue);
    }
    $scope.onDepartTime2Set = function(newValue, oldValue){
      $scope.item.route.parts[0].departureTime[1] = formatDateTime(newValue);
    }
    $scope.onArriveTime1Set = function(newValue, oldValue){
      $scope.item.route.parts[0].arrivalTime[0] = formatDateTime(newValue);
    }
    $scope.onArriveTime2Set = function(newValue, oldValue){
      $scope.item.route.parts[0].arrivalTime[1] = formatDateTime(newValue);
    }

    $scope.selectDepartureRange = function(isRangeSelected){
      if(!isRangeSelected)
        $scope.item.route.parts[0].departureTime.length = 1;
    }
    $scope.selectArrivalRange = function(isRangeSelected){
      if(!isRangeSelected)
        $scope.item.route.parts[0].arrivalTime.length = 1;
    }    

		$scope.saveOrder = function($event){
			$event.preventDefault();
			$event.stopPropagation();
			DataService.saveOrder($scope.item)
				.then(function(){
					$scope.setStep(6, $event);
				});
		};

    function init(){
      setInitialModel();
      $scope.step = 1;
      $scope.today = new Date();
      $scope.dateTypes = [{id:1, name: 'В'}, {id:2, name: 'Между'}];
      $scope.model = { 
        dateTypeFrom: $scope.dateTypes[0],
        dateTypeTo: $scope.dateTypes[0]
      };
      getCategories();
    }

    function getCategories(){
      CategoryService.getNonPassengerCategories()
        .then(function(data){
          $scope.categories = data;
          setAlreadySelectedCategory();
        })
    }

    function setAlreadySelectedCategory(){
      if($stateParams.categoryId){
        var selectedCategory = getCategoryById($stateParams.categoryId);
        if(!selectedCategory)
          return;

        $scope.step = 2;
        $scope.item.categoryId = selectedCategory.id
        $scope.item.category = selectedCategory.name

        getTags($stateParams.categoryId);
      }
    }

    function getCategoryById(id){
      var category = $scope.categories.filter(function(cat){
        return cat.id === id;
      });
      return category ? category[0] : null;
    }

    function getTags(categoryId){
      DataService.getTags(categoryId)
        .then(function(result){
          $scope.tags = result.data;
        })
    }

    function formatDateTime(value){
      return $filter('date')(value, "dd/MM/yy hh:mm");
    }

    function setInitialModel(){
      $scope.item = {
        id: 4,
        activeFor: {name: 'Актуально неделю', value: 7},
        isActive: true,
        categoryId: '',
        category: '',
        name: '',
        size: {
          width: '',
          length: '',
          depth: ''
        },
        weight: '',
        price: {
          expected: '',
          minimalBid: 0,
          blitz: 0
        },
        accessibleOnlyForPro: false,
        additionalInfo: {
          needsLoading: false,
          needsUnloading: false,
          needsPacking: false,
          needsBabyChair: false,
          hasServiceLift: false
        },
        auction: {
          expectedPrice: 700000,
          bidsCount: 25,
          minimalBid: 200000,
          blitzPrice: 500000,
          startDate: new Date(2015, 10, 17, 11, 0),
          endDate: new Date(2015, 10, 20, 11, 0)
        },
        route: {
          parts:[
            {
              from: {
                address: {
                  country: '',
                  city: '',
                  street: '',
                  house: '',
                  floor: '',
                  flat: ''
                }
              },
              departureTime: [],
              to: {
                address:{
                  country: '',
                  city: '',
                  street: '',
                  house: '',
                  floor: '',
                  flat: ''
                },
              },
              arrivalTime: [],
              length: '350'
            }
          ]
        }
      };
    }
	}

	angular.module('app').controller('orders.create.ship.ctrl', ['$scope', '$state', '$stateParams', 'DataService', 'CategoryService', 'CountryCityStreetService', '$filter', OrdersCreateShipCtrl]);
})();