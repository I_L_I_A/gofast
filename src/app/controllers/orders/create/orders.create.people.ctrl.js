(function () {

	'use strict';

	function OrdersCreatePeopleCtrl($scope, $state, DataService, CountryCityStreetService, $filter) {
		init();

		$scope.setStep = function (step, $event) {
			if(step == 4 && !$scope.validateStep3Form())
				return;

			$event.preventDefault();
			$event.stopPropagation();
			$scope.step = step;
		}

    $scope.searchCountries = function(search){
      CountryCityStreetService.getManyCountries(search).then(function(response){
        $scope.countries = response.data;
      });
    }
    $scope.searchCities = function(country, search){
      CountryCityStreetService.getManyCities(country, search).then(function(response){
        $scope.cities = response.data;
      });
    }
    $scope.searchStreets = function(country, city, search){
      CountryCityStreetService.getManyStreets(country, city, search).then(function(response){
        $scope.streets = response.data;
      });
    }

    $scope.selectDepartureRange = function(isRangeSelected){
      if(!isRangeSelected)
        $scope.order.route.departureTime.length = 1;
    }
    $scope.selectArrivalRange = function(isRangeSelected){
      if(!isRangeSelected)
        $scope.order.route.arrivalTime.length = 1;
    }    

		$scope.set2Form = function(form){
			$scope.step2Form = form;
			$scope.step2Form.count.minValue = 1;
			$scope.step2Form.count.maxValue = 10;
			$scope.step2Form.price.minValue = 0;
		};
		$scope.set3form = function(form){
			$scope.step3Form = form;
		};

		$scope.fieldHasError = function(field){
			return field.$invalid && field.$dirty;
		};

		$scope.getErrorText = function(field){
			if(field.$error['required'])
				return "Информация обязательна";
			if(field.$error['min'] || field.$error['max'])
				return "Введите число в диапазоне от " + field.minValue + " до " + (field.maxValue || '1 000 000 000');
		};

		$scope.setDirty = function(field){
			field.$setDirty();
		};

		$scope.submitStep3 = function(){
			$scope.step3submitted = true;
		};

		$scope.validateStep3Form = function(){
			if(!$scope.step3submitted)
				return true;

			var result = $scope.order.route.from.country && $scope.order.route.from.city
									&& $scope.order.route.to.country && $scope.order.route.to.city
									&& $scope.validateDatesFrom() && $scope.validateDatesTo();

			if(!result)
				return result;
			else
				return $scope.validateBothDates();
		};

		$scope.validateAddressFrom = function(){
			if(!$scope.step3submitted)
				return true;

			var address = $scope.order.route.from;
			return address.country && address.city;
		};

		$scope.validateAddressTo = function(){
			if(!$scope.step3submitted)
				return true;

			var address = $scope.order.route.to;
			return address.country && address.city;
		};

		$scope.validateDatesFrom = function(){
			var firstDate = $scope.order.route.departureTime[0];
			var secondDate = $scope.order.route.departureTime[1];

			if($scope.step3submitted && !$scope.step3Form.dateFrom1.$dirty && !firstDate && !secondDate){
				$scope.datesFromErrorText = "Информация обязательна";
				return false;
			}

			if($scope.model.dateTypeFrom.id == 1 && $scope.step3Form.dateFrom1.$dirty && !firstDate){
				$scope.datesFromErrorText = "Выберите дату";
				return false;
			}
			if($scope.model.dateTypeFrom.id == 2 && ($scope.step3Form.dateFrom1.$dirty || $scope.step3Form.dateFrom2.$dirty) && (!firstDate || !secondDate)){
				$scope.datesFromErrorText = "Выберите обе даты";
				return false;
			}
			if($scope.model.dateTypeFrom.id == 2 && ($scope.step3Form.dateFrom1.$dirty && $scope.step3Form.dateFrom2.$dirty) && firstDate && secondDate && (firstDate > secondDate)){
				$scope.datesFromErrorText = "Вторая дата должна быть больше первой";
				return false;
			}
			return true;
		};

		$scope.validateDatesTo = function(){
			var firstDate = $scope.order.route.arrivalTime[0];
			var secondDate = $scope.order.route.arrivalTime[1];

			if($scope.step3submitted && !$scope.step3Form.dateTo1.$dirty && !firstDate && !secondDate){
				$scope.datesToErrorText = "Информация обязательна";
				return false;
			}

			if($scope.model.dateTypeTo.id == 1 && $scope.step3Form.dateTo1.$dirty && !firstDate){
				$scope.datesToErrorText = "Выберите дату";
				return false;
			}
			if($scope.model.dateTypeTo.id == 2 && ($scope.step3Form.dateTo1.$dirty || $scope.step3Form.dateTo2.$dirty) && (!firstDate || !secondDate)){
				$scope.datesToErrorText = "Выберите обе даты";
				return false;
			}
			if($scope.model.dateTypeTo.id == 2 && ($scope.step3Form.dateTo1.$dirty && $scope.step3Form.dateTo2.$dirty) && firstDate && secondDate && (firstDate > secondDate)){
				$scope.datesToErrorText = "Вторая дата должна быть больше первой";
				return false;
			}
			return true;
		};

		$scope.validateBothDates = function(){
			if($scope.step3submitted && $scope.validateDatesFrom() && $scope.validateDatesTo()){
				if(!$scope.order.route.departureTime[1] && !$scope.order.route.arrivalTime[1])
					return $scope.order.route.departureTime[0] < $scope.order.route.arrivalTime[0];

				if($scope.order.route.departureTime[1] && !$scope.order.route.arrivalTime[1])
					return $scope.order.route.departureTime[1] < $scope.order.route.arrivalTime[0];

				if(!$scope.order.route.departureTime[1] && $scope.order.route.arrivalTime[1])
					return $scope.order.route.departureTime[0] < $scope.order.route.arrivalTime[0];

				if($scope.order.route.departureTime[1] && $scope.order.route.arrivalTime[1])
					return $scope.order.route.departureTime[1] < $scope.order.route.arrivalTime[1];
			} else
				return true;
		}

    function getTags(categoryId){ // здесь должна быть айдишка пассажирских перевозок
      DataService.getTags(categoryId)
        .then(function(result){
          $scope.tags = result.data;
        })
    }

    function init(){
			$scope.step = 2;
			$scope.item = {
				category: "Пассажирские перевозки"
			}
			$scope.order = {};
			$scope.order.route = { 
				from: {
					country: null,
					city: null,
					street: null,
					house: null
				},
				to: {
					country: null,
					city: null,
					street: null,
					house: null
				},
				departureTime: [], 
				arrivalTime: [] 
			};
    	getTags(1);

      $scope.dateTypes = [{id:1, name: 'В'}, {id:2, name: 'Между'}];
      $scope.model = { 
        dateTypeFrom: $scope.dateTypes[0],
        dateTypeTo: $scope.dateTypes[0]
      };
    }
	};

	angular.module('app').controller('orders.create.people.ctrl', ['$scope', '$state', 'DataService', 'CountryCityStreetService', '$filter', OrdersCreatePeopleCtrl]);
})();