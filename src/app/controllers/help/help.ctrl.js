(function () {

	'use strict';

  function HelpCtrl($scope, $state, $modalInstance, $http) {
  	$scope.close = function(){
      $modalInstance.close();
    }
    $scope.sendMessage = function (){
    	$http.post(API.HELP, {});
    }
  }
    
  angular.module('app').controller('help.ctrl', ['$scope','$state', '$modalInstance', '$http', HelpCtrl]);
})();