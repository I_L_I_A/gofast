(function () {

	'use strict';

  function HomeCtrl($scope, $state, $stateParams, DEFAULT_VALUES) {
    if($stateParams.driverView && $stateParams.driverView === 'true'){
      $scope.driverView = true;
    } else {
      $scope.driverView = false;
    }

  	$scope.goToCargoOrdersList = function(){
  		$state.go("app.orders.list", {
  			searchObj: {
		      cargoType: "1",//0 для пассажироперевозок
		      departure:{
		        date: new Date(),
		        country: DEFAULT_VALUES.DEPARTURE_COUNTRY,
		        city: DEFAULT_VALUES.DEPARTURE_CITY
		      },
		      arrival: {
		        country: DEFAULT_VALUES.ARRIVAL_COUNTRY,
		        city: DEFAULT_VALUES.ARRIVAL_CITY
		      },
		      isPassengerTransportation: false
    		}
    	});
  	}
  	$scope.goToPassengerOrdersList = function(){
  		$state.go("app.orders.list", {
  			searchObj: {
		      cargoType: "0",//0 для пассажироперевозок
		      departure:{
		        date: new Date(),
		        country: DEFAULT_VALUES.DEPARTURE_COUNTRY,
		        city: DEFAULT_VALUES.DEPARTURE_CITY
		      },
		      arrival: {
		        country: DEFAULT_VALUES.ARRIVAL_COUNTRY,
		        city: DEFAULT_VALUES.ARRIVAL_CITY
		      },
		      isPassengerTransportation: true
    		}
    	});
  	}
  }
    
  angular.module('app').controller('HomeCtrl', ['$scope','$state', '$stateParams', 'DEFAULT_VALUES', HomeCtrl]);
})();