(function () {

	'use strict';

  function ChangePasswordModalController($scope, $modalInstance, AuthService) {
  	$scope.changePassModel = {
  		oldPassword: '',
  		newPassword: '',
  		newPasswordRepeat: ''
  	}
  	$scope.close = function(){
  		$modalInstance.close();
  	}
    $scope.changePassword = function () {
        $scope.message = "";
 
        if (passwordsMatch()) {
            AuthService.changePassword($scope.changePassModel).then(function (response) {
              	$scope.close();
              }).catch(
              function (err) {
              	$scope.message = err.message;
                $scope.form.$submitted = false;
              });
        } else {
            $scope.message = "Пароли не совпадают!"
            $scope.form.$submitted = false;
        }
    };

    var passwordsMatch = function () {
        if ($scope.changePassModel.newPassword != $scope.changePassModel.newPasswordRepeat) {
            return false;
        }
        return true;
    };
  }
    
  angular.module('app').controller('changePassword.modal.ctrl', ['$scope', '$modalInstance', 'AuthService', ChangePasswordModalController]);
})();