(function () {

	'use strict';

  function LoginCtrl($scope, $state, $stateParams, AuthService, USER_ROLES) {
	  $scope.credentials = {
	    username: '',
	    password: ''
	  };

	  $scope.login = function () {
	    AuthService.login($scope.credentials.username, $scope.credentials.password)
		    .success(function(data){
		    	if($stateParams.wasRedirected){
		    		$state.go($stateParams.afterSignInState, $stateParams.afterSignInStateParameters);
		    		return;
		    	}
		    	if($scope.isAuthorized(USER_ROLES.CARRIER)){
						$state.go('app.profile.edit');
						return;
		    	}
		    	if($scope.isAuthorized(USER_ROLES.CUSTOMER)){
		    		$state.go('app.orders.list');
		    		return;
		    	}
		    });
	  };
  }
    
  angular.module('app').controller('LoginCtrl', ['$scope', '$state', '$stateParams', 'AuthService', 'USER_ROLES', LoginCtrl]);
})();