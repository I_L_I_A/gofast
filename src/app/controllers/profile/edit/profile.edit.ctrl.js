(function () {

	'use strict';

  function EditProfileCtrl($scope, $state, $modal,YEARS, MONTHS, PHONE_CODES) {
	  $scope.years = YEARS;
	  $scope.months = MONTHS;
	  $scope.phoneCodes = PHONE_CODES;

	  $scope.profile = $scope.currentUser.profileData;

	  $scope.getDaysInMonth = function(year, month){
	  	year = year? year: new Date().getFullYear();
	  	month = month? month: 1;
	  	var daysNumber = new Date(year, month, 0).getDate();
	  	var days = [];
	  	for(var i=0; i<daysNumber; i++){
	  		days.push(daysNumber - i);
	  	}
	  	return days;
	  };

	  $scope.openChangePasswordModal = function(){
	  	$modal.open({
          templateUrl: 'app/views/changePassword/changePassword.html',
          controller: 'changePassword.modal.ctrl'
      })
	  }

	  $scope.days = $scope.getDaysInMonth(new Date().getFullYear(), new Date().getMonth());

	  $scope.datepickerOptions = {
	  	maxDate: new Date()
	  };

    $scope.save = function(form){
    };
  }
    
  angular.module('app').controller('profile.edit.ctrl', ['$scope', '$state', '$modal', 'YEARS', 'MONTHS', 'PHONE_CODES', EditProfileCtrl]);
})();