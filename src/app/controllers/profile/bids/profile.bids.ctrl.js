(function () {

  'use strict';

  function BidsProfileCtrl($scope, $state, $modal, DataService, $filter, DATE_RANGES, OrderService) {
    $scope.bids = [];
    $scope.reverse = false;
    $scope.DATE_RANGES = DATE_RANGES;
    $scope.dateRange = 1;
    $scope.currentUser.authData.userId;//defined in app.ctrl.js
    getUserBids($scope.currentUser.authData.userId);
    
    $scope.sortBy = function (predicate, reverse) {
      $scope.reverse = reverse || !$scope.reverse;
      $scope.orders = $filter('orderBy')($scope.orders, predicate, $scope.reverse);
    };

    function getUserBids(userId) {
      OrderService.getCarrierBids(userId, $scope.dateRange)
        .success(function (bids) {
          $scope.bids = formatData(bids);
        })
    }

    $scope.leaveReviewForCustomer = function(customerId){
      $modal.open({
          template: '<gf-rating-form></gf-rating-form>',
          controller: 'profile.leaveReview.ctrl'
      })
    }

    function formatData(data) {
      angular.forEach(data, function (bid) {
        bid.order.departureTime = bid.order.route.departureTime[0];
        bid.order.arrivalTime = bid.order.route.arrivalTime[0];
        bid.order.routeLength = bid.order.route.length;
      })
      return data;
    }

    $scope.setDateRange = function (range, $event) {
      $event.preventDefault();
      $event.stopPropagation();
      $scope.dateRange = range;
      getUserBids($scope.currentUser.authData.userId, $scope.dateRange);
    }
    
  }

  angular.module('app').controller('profile.bids.ctrl', ['$scope', '$state', '$modal','DataService', '$filter', 'DATE_RANGES', 'OrderService', BidsProfileCtrl]);
})();