(function () {

	'use strict';

  function ProfileCtrl($scope, $state) {
	  $scope.$state = $state;
  }
    
  angular.module('app').controller('ProfileCtrl', ['$scope', '$state', ProfileCtrl]);
})();