(function () {

    'use strict';

    function CarrierCtrl($scope, $state, $stateParams, $filter, CarrierService) {
        $scope.$state = $state;
        $scope.activeMenu = false;
        $scope.newComment = getNewCommentModel();
        $scope.newCommentValidation = true;

        if ($stateParams.id) {
            CarrierService.get($stateParams.id).success(function(response) {
                $scope.carrier = response;
                if ($scope.carrier.cars.length > 0) {
                    $scope.activeAuto = $scope.carrier.cars[0];
                    if ($scope.activeAuto.imageUrls.length > 0) {
                        $scope.activeAutoImg =  $scope.activeAuto.imageUrls[0];
                    }
                }
            })
        }

        $scope.getAvailableAreasList = function(array) {
            if (array) {
                return array.join(', ');
            }

            return "";
        };

        $scope.getActiveAuto = function(index) {
            $scope.activeAuto = $scope.carrier.cars[index];
            if ($scope.activeAuto.imageUrls.length > 0) {
                $scope.activeAutoImg =  $scope.activeAuto.imageUrls[0];
            }
        };

        $scope.getActiveAutoImg = function(index) {
            if ($scope.activeAuto) {
                if ($scope.activeAuto.imageUrls.length > 0) {
                    $scope.activeAutoImg = $scope.activeAuto.imageUrls[index];
                }
            }
        };

        $scope.prevAutoImg = function() {
            if ($scope.activeAuto.imageUrls.length > 0 && $scope.activeAutoImg) {
                var index = _.findIndex($scope.activeAuto.imageUrls, function(img) {
                    return img.id == $scope.activeAutoImg.id;
                });
                if (index > 0) {
                    $scope.getActiveAutoImg(index - 1);
                }
            }
        }

        $scope.nextAutoImg = function() {
            if ($scope.activeAuto.imageUrls.length > 0 && $scope.activeAutoImg) {
                var index = _.findIndex($scope.activeAuto.imageUrls, function(img) {
                    return img.id == $scope.activeAutoImg.id;
                });
                if (index < $scope.activeAuto.imageUrls.length - 1) {
                    $scope.getActiveAutoImg(index + 1);
                }
            }
        }

        $scope.sortBy = function (predicate, reverse) {
            $scope.reverse = reverse || !$scope.reverse;
            $scope.carrier.transportations = $filter('orderBy')($scope.carrier.transportations, predicate, $scope.reverse);
        };

        $scope.setForm = function(form){
          $scope.newCommentForm = form;
        }

        $scope.fieldHasError = function(field){
          return field.$invalid && field.$dirty;
        }

        $scope.validateNewComment = function(){
          return $scope.newComment.header && $scope.newComment.text;
        }

        $scope.comment = function(header, text){
          $scope.submitted = true;
          $scope.newCommentForm.header.$setDirty();
          $scope.newCommentForm.text.$setDirty();

          if(!$scope.validateNewComment()){
            return;
          }

          var comment = {
            firstName: "Константин", 
            lastName: "Константинопольский", 
            header: header,
            ownRating: 50,
            rating: calculateRating(),
            shippings: 12,
            createdDateTime: new Date(),
            imgUrl: 'http://placehold.it/70x70', 
            text: text
          };
          $scope.carrier.comments.push(comment);
          $scope.newCommentForm.$setPristine(); // reset form validation
          $scope.newComment = getNewCommentModel(); //clear new comment model
          $scope.submitted = false;
        }

        function getNewCommentModel(){
          return {
            rating: {
              comfort: 0,
              responsibility: 0,
              punctuality: 0,
              politeness: 0
            },
            header: '',
            text: ''
          };
        }

        function calculateRating(){
          var rating = $scope.newComment.rating;
          return (rating.comfort+rating.responsibility+rating.punctuality+rating.politeness)/4;
        }
    }

    angular.module('app').controller('carrier.ctrl', ['$scope', '$state', '$stateParams', '$filter', 'CarrierService', CarrierCtrl]);
})();