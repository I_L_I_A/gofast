(function () {

  'use strict';

  function ProfileOrdersCtrl($scope, $state, DataService, $filter, DATE_RANGES, OrderService) {
    $scope.bids = [];
    $scope.reverse = false;
    $scope.DATE_RANGES = DATE_RANGES;
    $scope.dateRange = 1;
    $scope.currentUser.authData.userId;//defined in app.ctrl.js
    getUserBids($scope.currentUser.authData.userId);

    $scope.sortBy = function (predicate, reverse) {
      $scope.reverse = reverse || !$scope.reverse;
      $scope.orders = $filter('orderBy')($scope.orders, predicate, $scope.reverse);
    };

    function getUserBids(userId) {
      OrderService.getCarrierBids(userId, $scope.dateRange)
        .success(function (bids) {
          $scope.bids = formatData(bids);
        })
    }

    function formatData(data) {
      angular.forEach(data, function (bid) {
        bid.order.departureTime = bid.order.route.departureTime[0];
        bid.order.arrivalTime = bid.order.route.arrivalTime[0];
        bid.order.routeLength = bid.order.route.length;
      })
      return data;
    }

    $scope.setDateRange = function (range, $event) {
      $event.preventDefault();
      $event.stopPropagation();
      $scope.dateRange = range;
      getUserBids($scope.currentUser.authData.userId, $scope.dateRange);
    }
    
  }

  angular.module('app').controller('profile.orders.ctrl', ['$scope', '$state', 'DataService', '$filter', 'DATE_RANGES', 'OrderService', ProfileOrdersCtrl]);
})();