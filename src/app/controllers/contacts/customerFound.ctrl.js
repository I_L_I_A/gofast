(function () {

    'use strict';

    function CustomerFoundCtrl($scope, $state, $modalInstance, API, order) {
        $scope.close = function(){
            $modalInstance.close();
        };

        $scope.order = order;

        $scope.getLotLink = function() {
            if ($scope.order) {
                return API.ORDERS.GET + '?orderId=' + $scope.order.orderId;
            }
        };

        $scope.goToOrderDetails = function(){
            $state.go("app.orders.details", {orderId: order.id});
            $scope.close();
        }

        $scope.getCarrierLink = function() {
            if ($scope.order) {
                return API.CARRIER + '/' + $scope.order.carrierId;
            }
        };
    }

    angular.module('app').controller('customerFound.ctrl', ['$scope', '$state','$modalInstance', 'API', 'order', CustomerFoundCtrl]);
})();