(function () {

	'use strict';

  function CarrierChosenCtrl($scope, $state, $modalInstance, carrier) {
  	$scope.close = function(){
      $modalInstance.close();
    }
    $scope.carrier = carrier;
    $scope.goToCarrierProfile = function(){
    	$state.go("app.carrier", {id: carrier.id});
    	$scope.close();
    }
  }
    
  angular.module('app').controller('carrierChosen.ctrl', ['$scope','$state', '$modalInstance', 'carrier', CarrierChosenCtrl]);
})();