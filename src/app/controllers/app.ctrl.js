(function () {

  'use strict';

  function AppCtrl($rootScope, $state, $scope, $modal, USER_ROLES, AuthService, AUTH_EVENTS, CurrentUserService, OrderService, $interval) {
    $scope.USER_ROLES = USER_ROLES;
    $scope.currentUser = CurrentUserService.get();
    $scope.$state = $state;

    $scope.isAuthorized = function(userRole){
      return AuthService.isAuthorized(userRole);
    };

    $scope.isAuthenticated = function(){
      return AuthService.isAuthenticated();
    };

    $scope.logOut = function(){
      AuthService.logOut();
    };

    $scope.showHelp = function(){
      $modal.open({
          windowClass: '_help',
          templateUrl: 'app/views/help/help.html',
          controller: 'help.ctrl'
      })
    };
    $rootScope.$on(AUTH_EVENTS.getClientCredentialsSuccess, function (event) {
    });
    $rootScope.$on(AUTH_EVENTS.getClientCredentialsFailed, function (event) {
    });
    $rootScope.$on(AUTH_EVENTS.loginSuccess, function (event) {
      $scope.currentUser = CurrentUserService.get();
      CurrentUserService.getUserProfile();
      if (!$scope.orderChecker && $scope.isAuthorized(USER_ROLES.CARRIER)) {
        $scope.startCheckingOrders();
      }
    });
    $rootScope.$on(AUTH_EVENTS.getUserProfileSuccess, function (event) {
      $scope.currentUser = CurrentUserService.get();
    });
    $rootScope.$on(AUTH_EVENTS.getUserProfileFailed, function (event) {
      $scope.currentUser = CurrentUserService.get();
    });
    $rootScope.$on(AUTH_EVENTS.loginFailed, function (event) {
      $scope.currentUser = null;
    });
    $rootScope.$on(AUTH_EVENTS.logoutSuccess, function (event) {
      $scope.currentUser = null;
    });

    $scope.getCountYearsFromBirthday = function(bDate) {
      if (bDate) {
        return Math.floor(((new Date() - bDate) / 1000 / (60 * 60 * 24) ) / 365.25);
      }

      return "";
    };

    $scope.startCheckingOrders = function(){
      $scope.orderChecker = $interval(function(){
        OrderService.checkOrders($scope.currentUser.authData.userId).success(function(response) {
          if (response) {
            angular.forEach(response, function(order) {
              $modal.open({
                animation: true,
                templateUrl: 'app/views/contacts/customerFound.html',
                controller: 'customerFound.ctrl',
                resolve: {
                  order: function () {
                    return order;
                  }
                }
              });
            });
          }
        });
      }, 900000);
    }
    if (!$scope.orderChecker && $scope.isAuthorized(USER_ROLES.CARRIER)) {
      $scope.startCheckingOrders();
    }

  }

  angular.module('app').controller('AppCtrl', ['$rootScope', '$state', '$scope', '$modal', 'USER_ROLES', 'AuthService', 'AUTH_EVENTS', 'CurrentUserService', 'OrderService', '$interval',AppCtrl]);
})();