(function () {

  'use strict';

  function RegistrationCtrl($scope, $state, DataService, USER_ROLES, PHONE_CODES) {
    $scope.userData = {
      type: USER_ROLES.CARRIER,
      phoneCode: "375",

    };
    $scope.selectPhoneArea = 'BLR +375';
    $scope.phoneCodes = PHONE_CODES;

    $scope.setType = function (userType, $event) {
      $scope.userData.type = userType;
    };

    $scope.register = function (form) {
      if(form.$valid){
        DataService.validateUserInfo($scope.userData)
          .success(function(data){
            $state.go("app.registration.confirm");
            // $state.go("app.login");
          }).error(function(responce){
            var x = responce;
          })
      }
    };

    $scope.sendConfirmationCode = function (phone) {
      DataService.sendConfirmationCode(phone);
    };

    $scope.confirmCode = function (code) {
      DataService.completeRegister(code)
        .success(function(data){
          $scope.wrongCode = false;
          $state.go("app.login");
        })
        .catch(function(){
          $scope.wrongCode = true;
        })
    };

  }

  angular.module('app').controller('RegistrationCtrl', ['$scope', '$state', 'DataService', 'USER_ROLES', 'PHONE_CODES', RegistrationCtrl]);
})();