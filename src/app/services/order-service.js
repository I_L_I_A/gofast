(function () {

  'use strict';

  function OrderService($http, $filter, API ,CARGO_TYPES) {
    return {
      get: get,
      getMany: getMany,
      getOrderBids: getOrderBids,
      getCarrierBids: getCarrierBids,
      getCarrierBids: getCarrierBids,
      acceptBid: acceptBid,
      checkOrders: checkOrders
    };

    function get(orderId){
      return $http.get(API.ORDERS.GET, {
        params:{
          "orderId": orderId,
        }
      });
    }

    function getMany(searchObj){
      if(!searchObj && !searchObj.departure && !searchObj.arrival)
        return null;

      if(searchObj.cargoType == CARGO_TYPES.PASSENGERS){
        return $http.get(API.ORDERS.GET_MANY, {
          isArray: true,
          params:{
            "fromCountry": searchObj.departure.country,
            "fromCity": searchObj.departure.city,
            "toCountry": searchObj.arrival.country,
            "toCity": searchObj.arrival.city,
            "departureTime": $filter('date')(searchObj.departure.date, "yyyy-MM-dd"),
            "cargoType": searchObj.cargoType,
            "passengersQuantity": searchObj.passengersQuantity,
          }
        });
      } else {
        return $http.get(API.ORDERS.GET_MANY, {
          isArray: true,
          params:{
            "fromCountry": searchObj.departure.country,
            "fromCity": searchObj.departure.city,
            "toCountry": searchObj.arrival.country,
            "toCity": searchObj.arrival.city,
            "departureTime": $filter('date')(searchObj.departure.date, "yyyy-MM-dd"),
            "cargoType": searchObj.cargoType,
            "width": searchObj.width,
            "length": searchObj.length,
            "height": searchObj.heigth,
            // "volume": searchObj.volume //в случае если нужен объём
          }
        });
      }
    }

    function getOrderBids(orderId){
      return $http.get(API.ORDERS.GET_ORDER_BIDS, { 
        isArray: true,
        params:{ 
          "orderId": orderId 
        } 
      });
    }

    function getCarrierBids(userId){
      return $http.get(API.ORDERS.GET_CARRIER_BIDS, { 
        isArray: true,
        params:{ 
          "userId": userId 
        } 
      });
    }

    function checkOrders(carrierId) {
        return $http.get(API.ORDERS.CHECK_ORDERS + '?carrierId=' + carrierId);
    }


    function acceptBid(orderId, customerId, bidId){
      return $http.post(API.BIDS.ACCEPT, { 
        params:{
          customerId: customerId,
          orderId: orderId,
          bidId: bidId 
        } 
      });
    }
  }
    
  angular.module('app').factory('OrderService', ['$http', '$filter', 'API', 'CARGO_TYPES', OrderService]);
})();