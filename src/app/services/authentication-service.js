(function () {

  'use strict';

  function AuthService($rootScope, $q, $injector, $location, localStorageService, CurrentUserService, API, AUTH_EVENTS, OAUTH, USER_ROLES, LOCAL_STORAGE_KEYS) {

    var authenticated = false;
    loadAuthData();

    function loadAuthData() {
      var authData = localStorageService.get(LOCAL_STORAGE_KEYS.authorization);
      if(authData) 
        authenticated = true;
    }

    function getCurrentUserInfo(){
      var authData = localStorageService.get(LOCAL_STORAGE_KEYS.authorization);
      if(authData)
        return authData;
    }

    function login(loginName, password) {
      var data = 'grant_type=' + OAUTH.PASSWORD + '&username=' + loginName + '&password=' + password + '&client_id=' + OAUTH.ID + '&client_secret=' + OAUTH.SECRET;
      return $injector.get('$http').post(API.TOKEN, data, { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } })
        .success(function (data, status, headers, config) {
          var authData = {
            token: data.token,
            tokenType: data.tokenType,
            grantType: OAUTH.PASSWORD,
            userName: data.userName,
            userRole: data.userRole,
            refreshToken: data.refreshToken,
            refreshTokenTimestamp: new Date().getTime(),
            userId: data.userId
          }
          CurrentUserService.setAuthData(authData);
          authenticated = true;
          $rootScope.$broadcast(AUTH_EVENTS.loginSuccess, loginName);
        })
        .error(function (data, status, headers, config) {
          CurrentUserService.clear();
          $rootScope.$broadcast(AUTH_EVENTS.loginFailed, loginName);
        });
    }

    function logOut() {
      authenticated = false;
      CurrentUserService.clear();
      $rootScope.$broadcast(AUTH_EVENTS.logoutSuccess);
    }

    function changePassword() {
      var deferred = $q.defer();
      // $injector.get('$http').post(API.USERS.)
      deferred.resolve();
      return deferred.promise;
    }

    function isAuthenticated() {
      return authenticated;
    }

    function isAuthorized(authorizedRoles) {
      if (!angular.isArray(authorizedRoles)) {
        authorizedRoles = [authorizedRoles];
      }
      var currentUserInfo = getCurrentUserInfo();
      var currentUserRole = currentUserInfo ? currentUserInfo.userRole : null;
      
      return ( isAuthenticated() && authorizedRoles.indexOf(currentUserRole) !== -1 );
    }

    return {
      login: login,
      logOut: logOut,
      changePassword: changePassword,
      isAuthenticated: isAuthenticated,
      isAuthorized: isAuthorized,
      getCurrentUserInfo: getCurrentUserInfo
    };
  }
    
  angular.module('app').factory('AuthService', ['$rootScope', '$q', '$injector', '$location', 'localStorageService', 'CurrentUserService', 'API', 'AUTH_EVENTS', 'OAUTH', 'USER_ROLES', 'LOCAL_STORAGE_KEYS', AuthService]);

})();