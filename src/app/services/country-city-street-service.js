(function () {

  'use strict';

  function CountryCityStreetService($http, API) {
    return {
    	getManyCountries: getManyCountries,
    	getManyCities: getManyCities,
    	getManyStreets: getManyStreets 
    };

    function getManyCountries(search){
    	return $http.get(API.COUNTRIES.GET_MANY, {params:{"search": search}});
    }

    function getManyCities(country, search){
    	return $http.get(API.CITIES.GET_MANY, {params:{"country": country, "search": search}});
    }

    function getManyStreets(country, city, search){
    	return $http.get(API.STREETS.GET_MANY, {params:{"country": country, "city": city, "search": search}});
    }
  }
    
  angular.module('app').factory('CountryCityStreetService', ['$http', 'API', CountryCityStreetService]);
})();