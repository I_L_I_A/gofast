(function () {

    'use strict';

    function CarrierService($q, $http, API) {
        return {
            get: get
        };

        function get(carrierId){
            return $http.get(API.CARRIER, {
                params:{
                    "carrierId": carrierId
                }
            });
        }
    }

    angular.module('app').factory('CarrierService', ['$q', '$http', 'API', CarrierService]);
})();