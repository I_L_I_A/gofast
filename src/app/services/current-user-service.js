(function () {

'use strict';
function CurrentUserService ($rootScope, $injector, localStorageService, LOCAL_STORAGE_KEYS, API, AUTH_EVENTS) {
    var currentUser = {
        authData: null,
        profileData: null
    }

    function getUserProfile(){
        $injector.get('$http').get(API.USERS.GET_PROFILE+'?userId='+currentUser.authData.userId)
        .success(function (data, status, headers, config) {
          var profileData = data;
          setProfileData(profileData);
          $rootScope.$broadcast(AUTH_EVENTS.getUserProfileSuccess, profileData);
        })
        .error(function (data, status, headers, config) {
          localStorageService.remove(LOCAL_STORAGE_KEYS.userProfile);
          $rootScope.$broadcast(AUTH_EVENTS.getUserProfileFailed);
        });
    }
    function save() {
        if(currentUser.authData){
            localStorageService.set(LOCAL_STORAGE_KEYS.authorization, currentUser.authData);
        }
        if(currentUser.profileData){
            localStorageService.set(LOCAL_STORAGE_KEYS.userProfile, currentUser.profileData);
        }
    }

    function restoreFromLocalStorage() {
        if (localStorageService && localStorageService.get(LOCAL_STORAGE_KEYS.authorization) != null) {
            currentUser.authData = localStorageService.get(LOCAL_STORAGE_KEYS.authorization);
        } else {
            currentUser.authData = null;
        }
        if (localStorageService && localStorageService.get(LOCAL_STORAGE_KEYS.userProfile) != null) {
            currentUser.profileData = localStorageService.get(LOCAL_STORAGE_KEYS.userProfile);
        } else {
            currentUser.profileData = null;
        }
        return currentUser;
    }

    function setAuthData(authData) {
        currentUser = currentUser? currentUser: {};
        currentUser.authData = authData;
        save();
    }

    function setProfileData(profileData) {
        currentUser = currentUser? currentUser: {};
        currentUser.profileData = profileData;
        save();
    }

    function update(user) {
        currentUser = user; // or merge
        save();
    }

    function get() {
        if(!currentUser.authData && !currentUser.profileData){
            restoreFromLocalStorage();
        }
        return currentUser;
    }

    function getAuthData(){
        return currentUser? currentUser.authData: null;
    }

    function getProfileData(){
        return currentUser? currentUser.profileData: null;
    }

    function clear() {
        currentUser = null;
        localStorageService.remove(LOCAL_STORAGE_KEYS.authorization);
        localStorageService.remove(LOCAL_STORAGE_KEYS.userProfile);
    }

    function isAuthorized(role) {
        if (currentUser && currentUser.authData.role != null) {
            return currentUser.role.indexOf(role) != -1;
        }
        return false;
    }
    return {
      getUserProfile: getUserProfile,
      save: save,
      restoreFromLocalStorage: restoreFromLocalStorage,
      setAuthData: setAuthData,
      setProfileData: setProfileData,
      update: update,
      get: get,
      getAuthData: getAuthData,
      getProfileData: getProfileData,
      clear: clear,
      isAuthorized: isAuthorized,
    };
}
angular.module('app').service('CurrentUserService', ["$rootScope", "$injector", "localStorageService", "LOCAL_STORAGE_KEYS", "API", "AUTH_EVENTS", CurrentUserService]);
})();