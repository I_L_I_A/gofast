(function() {

  'use strict';

  function dataService($http, API) {
      
    return {
      validateUserInfo: validateUserInfo,
      completeRegister: completeRegister,
      sendConfirmationCode: sendConfirmationCode,
      login: login,
      getOrdersList: getOrdersList,
      saveOrder: saveOrder,
      getUserBids: getUserBids,
      getTags: getTags
    };
    
    function validateUserInfo(userInfo) {
      return $http.post(API.REGISTER.VALIDATE, userInfo, { 'Content-Type': 'application/x-www-form-urlencoded' });
    }

    function completeRegister(code) {
      return $http.post(API.COMPLETE_REGISTER, {code: code});
    }

    function login() {
      return $http.post(API.LOGIN);
    }

    function sendConfirmationCode(phone) {
      return $http.post(API.SEND_CODE, {phone: phone});
    }

    function getOrdersList() {
      return $http.get(API.ORDERS.GET_MANY);
    }
    
    function getUserBids(userId) {
      return $http.get(API.BIDS.GET_USER_BIDS, {userId: userId});
    }

    function saveOrder (order){
      return $http.post(API.ORDERS.SAVE, {order: order});
    }

    function getTags(categoryId){
      return $http.get(API.TAGS, {params: {categoryId: categoryId}});
    }
  }
    
  angular.module('app').factory('DataService', ['$http', 'API', dataService]);
})();