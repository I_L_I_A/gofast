(function () {

  'use strict';

  function BidService($resource, API) {
  	var service = $resource(API.BIDS.ROOT, {}, {
        getBid: {
          method: 'GET',
          url: API.BIDS.GET
        },
        getMany: {
          method: 'GET',
          url: API.BIDS.GET_MANY
        },
        addBid: {
          method: 'POST',
          url: API.BIDS.ADD
        },
        updateBid: {
          method: 'PUT',
          url: API.BIDS.UPDATE
        }
    });
    return service;
  }
    
  angular.module('app').factory('BidService', ['$resource', 'API', BidService]);
})();