(function () {

    'use strict';

    function notificationService() {

        return {
            success: success,
            error: error
        };

        function success(message) {
            toastr.success(message);
        }

        function error(message) {
            toastr.error(message);
        }
    }

    angular.module('app').factory('notificationService', notificationService);
})();