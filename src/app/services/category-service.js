(function () {

  'use strict';

  function CategoryService($q, $http, API) {
  	var categories = null;
    var nonPassengerCategories = null;


    function getAll(){
    	var deferred = $q.defer();

    	if(categories){
    		deferred.resolve(categories);
    		return deferred.promise;
    	}
    	$http.get(API.CATEGORIES.GET_ALL).success(function(data) {
    		categories = data;
    		deferred.resolve(data);
    	}).error(function(){
    		deferred.resolve(null);
    	});

    	return deferred.promise;
    }

    function getNonPassengerCategories(){
      var deferred = $q.defer();

      if(nonPassengerCategories){
        deferred.resolve(nonPassengerCategories);
        return deferred.promise;
      }

      getAll().then(function(allCategories){
        nonPassengerCategories = allCategories.filter(function(category){
          return !category.isPassenger;
        })
        deferred.resolve(nonPassengerCategories);
      }).catch(function(){
        deferred.resolve(null);
      });

      return deferred.promise;
    }

    getAll();
    getNonPassengerCategories();

    return {
    	getAll: getAll,
      getNonPassengerCategories: getNonPassengerCategories
    };
  }
    
  angular.module('app').factory('CategoryService', ['$q', '$http', 'API', CategoryService]);
})();