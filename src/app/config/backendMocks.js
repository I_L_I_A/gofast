(function () {
  angular
    .module('app')
    .run([
      '$httpBackend',
      'API',
      'CATEGORIES',
      function ($httpBackend, API, CATEGORIES) {
        // $httpBackend.whenGET().passThrough();
        // $httpBackend.whenPOST().passThrough();
        $httpBackend.whenGET(/\.html$/).passThrough(); // pass all requests to .html files

        $httpBackend.whenPOST(API.LOGIN).respond(200);

        $httpBackend.whenPOST(API.REGISTER.VALIDATE).respond(200);

        $httpBackend.whenPOST(API.REGISTER.FINALIZE).respond(function (method, url, data) {
          var code = angular.fromJson(data).code;
          return code === "1111" ? [200] : [500];
        });

        $httpBackend.whenPOST(API.TOKEN).respond(function (method, url, data) {
          var authData = null;
          if (data.indexOf('alex') > -1) {
            authData = {
              token: '1sa97bsxblwk88c4o80000kcsoccsgg4owgcg440scsc8w0gkk',
              tokenType: 'Bearer',
              userName: 'alex',
              userRole: 'customer',
              refreshToken: '1sa97bsxblwk88c4o80000kcsoccsgg4owgcg440scsc8w0gkk',
              userId: '2'
            }
          } else {
            authData = {
              token: '1sa97bsxblwk88c4o80000kcsoccsgg4owgcg440scsc8w0gkk',
              tokenType: 'Bearer',
              userName: 'Peter',
              userRole: 'carrier',
              refreshToken: '1sa97bsxblwk88c4o80000kcsoccsgg4owgcg440scsc8w0gkk',
              userId: '1'
            };
          }
          return [200, authData, {}];
        });
        var categories = CATEGORIES;
        var orders = {
          totalItems: 3,
          totalPages: 1,
          page: 1,
          result: [
            {
              id: 1,
              creationDate: new Date(2015, 10, 14, 14, 0),
              name: "Домашние коты",
              description: "Приятные создания",
              accessibleOnlyForPro: true,
              userId: 2,
              userFirstName: "Борис",
              userLastName: "Севастопольский",
              isActive: true,
              views: 660,
              category: "Домашние животные",
              categoryId: "0",
              categoryImage: "/img/typ1.png",
              price: {
                expected: 300000,
                minimalBid: 200000,
                blitz: 500000
              },
              weight: 20,
              size: {
                width: 200,
                length: 150,
                depth: 100
              },
              route: {
                from: {
                  address: {
                    country: "Беларусь",
                    city: "Барановичи",
                    street: "Бурдейного",
                    house: 35,
                    floor: 7,
                    flat: 84
                  },
                  coordinate: {
                    lat: 53.125574,
                    long: 26.009168
                  }
                },
                departureTime: [new Date(2015, 10, 27, 14, 0), new Date(2015, 10, 28, 10, 0)],
                to: {
                  address: {
                    country: "Беларусь",
                    city: "Старые дороги",
                    street: "Чайлытко",
                    house: 22,
                    floor: 3,
                    flat: 44
                  },
                  coordinate: {
                    lat: 53.038990,
                    long: 28.260550
                  }                      
                },
                arrivalTime: [new Date(2015, 10, 17, 16, 0)],
                length: 100
              },
              bidsCount: 2,
              image: "http://placehold.it/300x300",
              needsLoading: true,
              needsUnloading: false,
              needsPacking: true,
              needsBabyChair: false,
              hasServiceLift: true,
              additionalInfo: {
                needsLoading: true,
                needsUnloading: false,
                needsPacking: true,
                needsBabyChair: false,
                hasServiceLift: true
              },
              views: 12
            },
            {
              id: 2,
              creationDate: new Date(2015, 10, 15, 11, 0),
              name: "Комод",
              description: "Очень тяжёлый",
              accessibleOnlyForPro: true,
              isActive: true,
              userId: 5,
              userFirstName: "Борис",
              userLastName: "Севастопольский",
              views: 660,
              category: "Мебель",
              categoryImage: "/img/typ1.png",
              price: {
                expected: 500000,
                minimalBid: 500000,
                blitz: 700000
              },
              weight: 200,
              size: {
                width: 200,
                length: 150,
                depth: 100
              },
              route: {
                from: {
                  address: {
                    country: "Беларусь",
                    city: "Молодечно",
                    street: "Пер. Броневой",
                    house: 10,
                    floor: 1,
                    flat: 2
                  },
                  coordinate: {
                    lat: 54.310410,
                    long: 26.848882
                  }                      
                },
                departureTime: [new Date(2015, 10, 18, 23, 0)],
                to: {
                  address: {
                    country: "Беларусь",
                    city: "Витебск",
                    street: "Куриновича",
                    house: 22,
                    floor: 3,
                    flat: 44
                  },
                  coordinate: {
                    lat: 55.184806,
                    long: 30.201622
                  }  
                },
                arrivalTime: [new Date(2015, 10, 19, 5, 0)],
                length: 400
              },
              bidsCount: 2,
              image: "http://placehold.it/300x300",//server will generate a specific link to API which client will use.
              needsLoading: true,
              needsUnloading: false,
              needsPacking: true,
              needsBabyChair: false,
              hasServiceLift: true,
              additionalInfo: {
                needsLoading: true,
                needsUnloading: false,
                needsPacking: true,
                needsBabyChair: false,
                hasServiceLift: true
              },
              views: 4
            },
            {
              id: 3,
              creationDate: new Date(2015, 10, 15, 11, 0),
              name: "Рояль",
              description: "Очень классный бабушкин рояль",
              accessibleOnlyForPro: true,
              userId: 5,
              userFirstName: "Борис",
              userLastName: "Севастопольский",
              isActive: false,
              views: 660,
              category: "Крупно-габаритные предметы",
              price: {
                expected: 700000,
                minimalBid: 500000,
                blitz: 800000
              },
              weight: 200,
              size: {
                width: 200,
                length: 150,
                depth: 100
              },
              route: {
                from: {
                  address: {
                    country: "Беларусь",
                    city: "Гродно",
                    street: "Чайлытко",
                    house: 35,
                    floor: 7,
                    flat: 84
                  },
                  coordinate: {
                    lat: 53.669354,
                    long: 23.813131
                  }    
                },
                departureTime: [new Date(2015, 10, 18, 11, 0)],
                to: {
                  address: {
                    country: "Беларусь",
                    city: "Брест",
                    street: "Ленина",
                    house: 22,
                    floor: 3,
                    flat: 44
                  },
                  coordinate: {
                    lat: 52.097621,
                    long: 23.734050
                  }   
                },
                arrivalTime: [new Date(2015, 10, 18, 15, 0)],
                length: 300
              },
              image: "http://placehold.it/300x300",//server will generate a specific link to API which client will use.
              bidsCount: 2,
              needsLoading: true,
              needsUnloading: false,
              needsPacking: true,
              needsBabyChair: false,
              hasServiceLift: true,
              additionalInfo: {
                needsLoading: true,
                needsUnloading: false,
                needsPacking: true,
                needsBabyChair: false,
                hasServiceLift: true
              },
              views: 3
            }
          ]
        };

        $httpBackend.whenGET(new RegExp(API.ORDERS.GET_ORDER_BIDS + '\\?orderId=\(.+)')).respond(function () {
          var orderBids = [
            {
              carrier: {
                id: 1,
                isPro: true,
                firstName: "Иван",
                rating: "80",
                completedOrders: 33,
                car: {
                  id: 2,
                  modelName: "Peugeot Partner",
                  productionYear: 2014,
                  imageUrl: "http://placehold.it/50x50"
                },
                imageUrl: "http://placehold.it/50x50"
              },
              pickUpDate: new Date(2015, 10,26, 5,0),
              dropDate: new Date(2015, 10,28, 5,0),
              orderId: 1,
              description: "Сделаю всё быстро и качественно",
              price: 200000,
              loading: true,
              packing: true,
              date: new Date(2015, 8, 11, 5, 33)
            },
            {
              carrier: {
                id: 2,
                isPro: false,
                firstName: "Пётр",
                rating: "57",
                completedOrders: 12,
                car: {
                  id: 2,
                  modelName: "Toyota Landcruiser",
                  productionYear: 2015,
                  imageUrl: "http://placehold.it/50x50"
                },
                imageUrl: "http://placehold.it/50x50"
              },
              pickUpDate: new Date(2015, 10,26, 5,0),
              dropDate: new Date(2015, 10,28, 5,0),
              orderId: 1,
              description: "Сделаю всё быстро и качественно",
              price: 220000,
              loading: false,
              packing: true,
              date: new Date(2015, 8, 10, 17, 33)
            }
          ];
          return [200, orderBids, {}];
        });

        $httpBackend.whenGET(new RegExp(API.ORDERS.CHECK_ORDERS + '\\?carrierId=\(.+)')).respond(function() {
          var orders = [{
            customer: {
              firstName: 'Сергей',
              lastName: 'Иванов',
              imgUrl: 'http://placehold.it/150x150',
              ordersCount: 345,
              phoneCode: '375',
              phone: '295053587'
            },
            orderId: 1,
            carrierId: 1
          }
          ];
          return [200, orders, {}];
        });

        $httpBackend.whenGET(new RegExp(API.ORDERS.GET_CARRIER_BIDS)).respond(function () {
          var userBids = [
            {
              order: orders.result[0],
              bid: 2500000
            },
            {
              order: orders.result[1],
              bid: 500000
            },
            {
              order: orders.result[2],
              bid: 1250000
            }
          ];
          return [200, userBids, {}];
        });

        $httpBackend.whenGET(new RegExp(API.USERS.GET_PROFILE + '\\?userId=\(.+)')).respond(function (method, url, data) {
          var userProfile = null;
          if (url.indexOf("userId=2") > -1) {
            userProfile = {
              firstName: "Кирилл",
              lastName: "Иванов",
              birthday: new Date(1987, 10, 2),
              phoneCode: "375",
              phone: "295023587",
              imageUrl: "",
              rating: 76,
              unreadMessages: 0,
              orderCategories: CATEGORIES
            }
          } else {
            userProfile = {
              firstName: "Константин",
              lastName: "Константинопольский",
              birthday: new Date(1990, 10, 2),
              phoneCode: "375",
              phone: "295053587",
              imageUrl: "",
              rating: 88,
              unreadMessages: 0,
              orderCategories: CATEGORIES,
              cars: [
                {
                  id: 1,
                  modelName: "Toyota Landcruiser",
                  productionYear: 2015,
                  imageUrl: "http://placehold.it/50x50"
                },
                {
                  id: 2,
                  modelName: "Peugeot Partner",
                  productionYear: 2014,
                  imageUrl: "http://placehold.it/50x50"
                }
              ],
            }
          }

          return [200, userProfile, {}];
        });

        $httpBackend.whenPOST(new RegExp(API.BIDS.ADD)).respond(function() {
          return [200, {}, {}];
        });

        $httpBackend.whenPOST(new RegExp(API.BIDS.ACCEPT)).respond(function() {
          var carrierInfo = 
          {
            id: 1,
            isPro: true,
            ordersCount: 20,
            imageUrl: "http://placehold.it/150x150",
            firstName: "Константин",
            lastName: "Константинопольский",
            phoneCode: "375",
            phone: "295053587",
            car: {
              id: 1,
              modelName: "Peugeot Partner",
              productionYear: 2014,
              imageUrl: "http://placehold.it/40x40"
            }
          }
          return [200, carrierInfo, {}];
        });

        $httpBackend.whenPOST(new RegExp(API.BIDS.UPDATE)).respond(function() {
          return [200, {}, {}];
        });

        $httpBackend.whenGET(new RegExp(API.COUNTRIES.GET_MANY)).respond(function() {
          return [200, [{id: 1, name: "Беларусь"}, {id: 2, name: "Украина"}, {id: 3, name: "Россия"}], {}];
        });

        $httpBackend.whenGET(API.ORDERS.GET_MANY).respond(function () {
            return [200, orders, {}];
          });

        $httpBackend.whenGET(new RegExp(API.CITIES.GET_MANY)).respond(function() {
          return [200, [{id: 1, area: "Минский район", region: "Минская область", name:"Минск"}, {id: 2, area: "Брестский район", region: "Брестская область", name: "Брест"}, {id: 2, area: "Московский район", region: "Московская область", name: "Москва"}], {}];
        });

        $httpBackend.whenGET(new RegExp(API.STREETS.GET_MANY)).respond(function() {
          return [200, [{id: 1, name: "Куйбышева"}, {id: 2, name: "Ленина"}, {id: 3, name: "Притыцкого"}], {}];
        });

        $httpBackend.whenGET(new RegExp(API.CATEGORIES.GET_ALL)).respond(function() {
          return [200, categories, {}];
        });

        $httpBackend.whenGET(new RegExp(API.TAGS)).respond(function(method, url, data) {
          var categoryId = url.split('=')[1];
          var tags;
          if(categoryId <= 3){
            tags = ['Холодильник', 'Диван', 'Шкаф', 'Раковина', 'Тумба'];
            return [200, tags, {}];
          }
          if(categoryId <= 7){
            tags = ['Машина', 'Стол', 'Кровать', 'Дядя Толик', 'Парикмахерская'];
            return [200, tags, {}];
          }
          if(categoryId <= 11){
            tags = ['Слон', 'Жираф', 'Батарея', 'Хачапури', 'Мексиканец'];
            return [200, tags, {}];
          }
        });

        $httpBackend.whenPOST(API.ORDERS.SAVE).respond(function (method, url, data) {
          var order = angular.fromJson(data).order;
          orders.result.push(order);
          orders.totalItems = orders.totalItems++;
          return [200, order, {}];
        });

        $httpBackend.whenGET(new RegExp(API.ORDERS.GET)).respond(function (method, url, data, headers, params) {
          var orderId = url.split('=')[1];
          var order = orders.result.filter(function (item) {
            return item.id == orderId;
          })[0];
          return [200, order, {}];
        });

        $httpBackend.whenGET(new RegExp(API.CARRIER + '\\?carrierId=\(.+)')).respond(function() {
          var carrier = {
              userProfile : {
                id : 1,
                firstName: "Константин",
                lastName: "Константинопольский",
                birthday: new Date(1990, 2, 2),
                orders: 12563,
                notes: 'Привет, я работаю 40 лет в этом бизнесе. и уже 2ое поколение нашей семьи кто занимается грузоперевозками. Я осуществляю все виды перевозок, включая антиквариат. Я так же могу перевозить крупного габаритные. Могу помогать в погрузке и выгрузке',
                phoneCode: "375",
                phone: "295053587",
                imageUrl: "http://placehold.it/150x150",
                rating: 88
              },
              orderCategories: CATEGORIES,
              areasOfTrips: ["Беларусь", "Украина", "Россия"],
              servicesProvided: [ { serviceType: 1, serviceName: "Услуги грузчиков" }, {serviceType: 2, serviceName: "Услуги по упаковке"}, {serviceType: 3, serviceName: "Детское кресло"}],
              cars: [ {
                id: 1,
                name: "Автомобиль Газель",
                model: "Европлатформа",
                year: 1990,
                load: 12000,
                bodyType: 'Тент',
                loadingMethod: 'Задняя',
                imageUrls: [{id: 1, url: 'http://placehold.it/237x195'}, {id: 2, url: 'http://placehold.it/237x195'}, {id: 3, url: 'http://placehold.it/237x195'}],
                size: {
                  width: 200,
                  length: 150,
                  depth: 100
                },
                notes: "Новый Chevrolet Aveo 2012 модельного года предстал перед публикой и журналистами на Парижском мотор-шоу в октябре 2010 года. Автомобиль выпускается на универсальной платформе Gamma II в кузове седан, 3- и 5-дверный хетчбэк."
              }, {
                id: 2,
                name: "Автомобиль Газель",
                model: "Европлатформа",
                year: 1991,
                load: 12000,
                bodyType: 'Тент',
                loadingMethod: 'Задняя',
                imageUrls: [{id: 1, url: 'http://placehold.it/237x195'}, {id: 2, url: 'http://placehold.it/237x195'}, {id: 3, url: 'http://placehold.it/237x195'}],
                size: {
                  width: 200,
                  length: 150,
                  depth: 100
                },
                notes: "Новый Chevrolet Aveo 2012 модельного года предстал перед публикой и журналистами на Парижском мотор-шоу в октябре 2010 года. Автомобиль выпускается на универсальной платформе Gamma II в кузове седан, 3- и 5-дверный хетчбэк."
              } ],
              transportations: [
              {
                id: 3,
                creationDate: new Date(2015, 10, 15, 11, 0),
                name: "Рояль",
                description: "Очень классный бабушкин рояль",
                accessibleOnlyForPro: true,
                userId: 5,
                userFirstName: "Борис",
                userLastName: "Севастопольский",
                isActive: true,
                views: 660,
                category: "Крупно-габаритные предметы",
                price: {
                  expected: 750000,
                  minimalBid: 500000,
                  blitz: 800000
                },
                auction: {
                  expectedPrice: 700000,
                  bidsCount: 25,
                  minimalBid: 200000,
                  blitzPrice: 500000,
                  startDate: new Date(2015, 10, 17, 11, 0),
                  endDate: new Date(2015, 10, 20, 11, 0)
                },
                weight: 200,
                size: {
                  width: 200,
                  length: 150,
                  depth: 100
                },
                route: {
                  parts: [
                    {
                      from: {
                        address: {
                          country: "Беларусь",
                          city: "Минск",
                          street: "Чайлытко",
                          house: 35,
                          floor: 7,
                          flat: 84
                        },
                        coordinate: {
                          lat: 53.669354,
                          long: 23.813131
                        }
                      },
                      departureTime: [new Date(2015, 10, 18, 11, 0)],
                      to: {
                        address: {
                          country: "Беларусь",
                          city: "Брест",
                          street: "Ленина",
                          house: 22,
                          floor: 3,
                          flat: 44
                        },
                        coordinate: {
                          lat: 52.097621,
                          long: 23.734050
                        }
                      },
                      arrivalTime: [new Date(2015, 10, 18, 15, 0)],
                      length: 350
                    }
                  ]
                },
                image: "../assets/images/animal_icon.png",//server will generate a specific link to API which client will use.

                needsLoading: true,
                needsUnloading: false,
                needsPacking: true,
                needsBabyChair: false,
                hasServiceLift: true,
                additionalInfo: {
                  needsLoading: true,
                  needsUnloading: false,
                  needsPacking: true,
                  needsBabyChair: false,
                  hasServiceLift: true
                },
                views: 3
              }, {
                  id: 4,
                  creationDate: new Date(2015, 10, 15, 11, 0),
                  name: "Рояль",
                  description: "Очень классный бабушкин рояль",
                  accessibleOnlyForPro: true,
                  userId: 5,
                  userFirstName: "Борис",
                  userLastName: "Севастопольский",
                  isActive: true,
                  views: 660,
                  category: "Крупно-габаритные предметы",
                  price: {
                    expected: 700000,
                    minimalBid: 500000,
                    blitz: 800000
                  },
                  auction: {
                    expectedPrice: 700000,
                    bidsCount: 25,
                    minimalBid: 200000,
                    blitzPrice: 500000,
                    startDate: new Date(2015, 10, 17, 11, 0),
                    endDate: new Date(2015, 10, 20, 11, 0)
                  },
                  weight: 200,
                  size: {
                    width: 200,
                    length: 150,
                    depth: 100
                  },
                  route: {
                    parts: [
                      {
                        from: {
                          address: {
                            country: "Беларусь",
                            city: "Гродно",
                            street: "Чайлытко",
                            house: 35,
                            floor: 7,
                            flat: 84
                          },
                          coordinate: {
                            lat: 53.669354,
                            long: 23.813131
                          }
                        },
                        departureTime: [new Date(2015, 10, 18, 11, 0)],
                        to: {
                          address: {
                            country: "Беларусь",
                            city: "Брест",
                            street: "Ленина",
                            house: 22,
                            floor: 3,
                            flat: 44
                          },
                          coordinate: {
                            lat: 52.097621,
                            long: 23.734050
                          }
                        },
                        arrivalTime: [new Date(2015, 10, 18, 15, 0)],
                        length: 300
                      }
                    ]
                  },
                  image: "../assets/images/animal_icon.png",//server will generate a specific link to API which client will use.

                  needsLoading: true,
                  needsUnloading: false,
                  needsPacking: true,
                  needsBabyChair: false,
                  hasServiceLift: true,
                  additionalInfo: {
                    needsLoading: true,
                    needsUnloading: false,
                    needsPacking: true,
                    needsBabyChair: false,
                    hasServiceLift: true
                  },
                  views: 3
                }],
              comments: [
                {
                  firstName: "Николай", 
                  lastName: "Скворцов", 
                  header: "Всё очень круто!",
                  ownRating: 56,
                  rating: 80,
                  shippings: 12,
                  createdDateTime: new Date(2015, 10, 28, 15, 20),
                  imgUrl: 'http://placehold.it/70x70', 
                  text: "Я думаю результаты будут неплохими, если они будут знать, что в их сторону будут направлены АК. Хотя понятное дело, что новость очередная новостная утка."
                },
                {
                  firstName: "Андрей", 
                  lastName: "Иванов", 
                  header: "Не лучшая поездка, но и не худшая",
                  rating: 69,
                  ownRating: 56,
                  shippings: 4,
                  createdDateTime: new Date(2015, 10, 30, 11, 30),                  
                  imgUrl: 'http://placehold.it/70x70', 
                  text: "Андрей - веждиввый молодой человек, однако, к сожалени, за рулём он был пяьным, и по дороге нюхал порох."
                }
              ]
          };
          return [200, carrier, {}]
        });
      }
    ]);
})();
