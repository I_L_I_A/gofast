(function () {
  angular
    .module('app')
    .config([
      '$stateProvider',
      '$urlRouterProvider',
      'USER_ROLES',
      'DEFAULT_VALUES',
      function ($stateProvider, $urlRouterProvider, USER_ROLES, DEFAULT_VALUES) {

        $urlRouterProvider.otherwise(function ($injector, $location) {
          var $state = $injector.get('$state');
          $state.go('app.home');
        });

        $stateProvider
          .state('app', {
            templateUrl: 'app/views/layout/app.html',
            abstract: true,
            data: {
              requiredAuthentication: false,
              roles: [USER_ROLES.CARRIER, USER_ROLES.CUSTOMER]
            }
          })
          .state('app.registration', {
            url: '/registration',
            templateUrl: 'app/views/registration/main.html',
            controller: 'RegistrationCtrl',
            abstract: true,
          })
          .state('app.registration.new', {
            url: '/new',
            templateUrl: 'app/views/registration/new.html',
          })
          .state('app.registration.confirm', {
            url: '/confirm',
            templateUrl: 'app/views/registration/confirm.html',
          })
          .state('app.registration.editConfirm', {
            url: '/editConfirm',
            templateUrl: 'app/views/registration/editConfirm.html',
          })
          .state('app.profile', {
            url: '/profile',
            templateUrl: 'app/views/profile/base.html',
            abstract: true,
            controller: 'ProfileCtrl',
            data: {
              requiredAuthentication: true
            }
          })
          .state('app.profile.main', {
            url: '/main',
            templateUrl: 'app/views/profile/main.html',
          })
          .state('app.profile.transport', {
            url: '/transport',
            templateUrl: 'app/views/profile/transport/base.html',
            abstract: true,
          })
          .state('app.profile.transport.add', {
            url: '/add',
            templateUrl: 'app/views/profile/transport/add.html',
          })
          .state('app.profile.transport.list', {
            url: '/list',
            templateUrl: 'app/views/profile/transport/list.html',
          })
          .state('app.profile.bids', {
            url: '/bids',
            templateUrl: 'app/views/profile/bids.html',
            controller: 'profile.bids.ctrl',
             data: {
              requiredAuthentication: true,
              roles: [USER_ROLES.CARRIER]
            }
          })
          .state('app.profile.orders', {
            url: '/orders',
            templateUrl: 'app/views/profile/orders.html',
            controller: 'profile.orders.ctrl',
             data: {
              requiredAuthentication: true,
              roles: [USER_ROLES.CUSTOMER]
            }
          })
          .state('app.profile.messages', {
            url: '/messages',
            templateUrl: 'app/views/profile/messages.html',
          })
          .state('app.profile.reviews', {
            url: '/reviews',
            templateUrl: 'app/views/profile/reviews.html',
          })
          .state('app.profile.edit', {
            url: '/edit',
            templateUrl: 'app/views/profile/edit.html',
            controller: 'profile.edit.ctrl'
          })
          .state('app.profile.documents', {
            url: '/documents',
            templateUrl: 'app/views/profile/documents.html',
          })
            .state('app.carrier', {
              url: '/carrier/:id',
              templateUrl: 'app/views/profile/carrier.html',
              controller: 'carrier.ctrl'
            })
          .state('app.orders', {
            url: '/orders',
            templateUrl: 'app/views/orders/main.html',
            controller: 'orders.ctrl',
            abstract: true,
          })
          .state('app.orders.list', {
            url: '/list',
            controller: 'orders.list.ctrl',
            templateUrl: 'app/views/orders/list.html',
            params: {
              searchObj: {
                cargoType: "1",//0 для пассажироперевозок
                departure:{
                  date: new Date(),
                  country: DEFAULT_VALUES.DEPARTURE_COUNTRY,
                  city: DEFAULT_VALUES.DEPARTURE_CITY
                },
                arrival: {
                  country: DEFAULT_VALUES.ARRIVAL_COUNTRY,
                  city: DEFAULT_VALUES.ARRIVAL_CITY
                },
                isPassengerTransportation: false
              }
            }
          })
          .state('app.orders.details', {
            url: '/:orderId',
            controller: 'orders.details.ctrl',
            templateUrl: 'app/views/orders/details.html',
          })
          .state('app.orders.bid', {
            url: '/bid/:orderId',
            controller: 'orders.bid.ctrl',
            templateUrl: 'app/views/orders/bid.html',
            params:{
              order: null,
              editMode: false,
              bid: null
            },
            data: {
              requiredAuthentication: true,
              roles: [USER_ROLES.CARRIER]
            }
          })
           .state('app.orders.create', {
            url: '/create',
            templateUrl: 'app/views/orders/create/main.html',
            controller: 'orders.create.ctrl',
            abstract: true
          })
          .state('app.orders.create.ship', {
            url: '/ship',
            templateUrl: 'app/views/orders/create/ship.html',
            controller: 'orders.create.ship.ctrl',
            params: {
              categoryId: null
            }
          })
          .state('app.orders.create.people', {
            url: '/people',
            templateUrl: 'app/views/orders/create/people.html',
            controller: 'orders.create.people.ctrl',
          })
          .state('app.login', {
            url: '/login',
            templateUrl: 'app/views/login/login.html',
            controller: 'LoginCtrl',
            params: {
                afterSignInState: "app.home",
                afterSignInStateParameters: null,
                wasRedirected: false
            }
          })
          .state('app.home', {
            url: '/main?driverView',
            templateUrl: 'app/views/landing/home.html',
            controller: 'HomeCtrl',
          });
      }
    ]);
})();