(function() {
  var debug = false;
  angular
    .module('app')
    .constant('USER_ROLES', {
      ADMIN: 'admin',
      CARRIER: 'carrier',
      CUSTOMER: 'customer',
      GUEST: 'guest'
    })
    .constant('OAUTH', {
      ID: '1_3re822r00easwcskssgswgsw0sossggs48oos8088ocss0og4g',
      SECRET: '1sa97bsxblwk88c4o80000kcsoccsgg4owgcg440scsc8w0gkk',
      CLIENT_CREDENTIALS: 'client_credentials',
      PASSWORD: 'password',
      REFRESH_TOKEN: 'refresh_token'
    })
    .constant('API', (function() {
      var localDomain = "http://localhost:62701";
      var prodDomain = "http://gofast.by";
      var domain = debug? localDomain: prodDomain;
      var apiRoot = domain + '/api/v1';
      var apiBidsRoot = apiRoot + '/bids';
      var apiOrdersRoot = apiRoot + '/orders';
      var apiCarrier = apiRoot + '/carrier';
      var getOrdersBids = apiOrdersRoot + '/getOrderBids';//yes, APP.ORDERS.GET_ORDER_BIDS = APP.BIDS.GET_ORDER_BIDS
      var getCarrierBids = apiOrdersRoot + '/getCarrierBids';//yes, APP.ORDERS.GET_ORDER_BIDS = APP.BIDS.GET_ORDER_BIDS
      var api = {
        SERVER: domain,
        ROOT: apiRoot,
        CARRIER: apiCarrier,
        ORDERS: {
          ROOT: apiOrdersRoot,
          GET: apiOrdersRoot + '/get',
          GET_MANY: apiOrdersRoot + '/getMany',
          GET_ORDER_BIDS: getOrdersBids,
          GET_CARRIER_BIDS: apiOrdersRoot + '/getCarrierBids',
          SAVE: apiOrdersRoot + '/save',
          CHECK_ORDERS: apiOrdersRoot + '/checkOrders'
        },
        BIDS: {
          ROOT: apiBidsRoot,
          GET: apiBidsRoot + '/get',
          GET_MANY: apiBidsRoot + '/getMany',
          GET_ORDER_BIDS: getOrdersBids,
          GET_USER_BIDS: apiBidsRoot + '/getUserBids',
          ADD: apiBidsRoot + '/add',
          UPDATE: apiBidsRoot + '/update',
          ACCEPT: apiBidsRoot + '/accept'
        },
        USERS: {
          GET_PROFILE: domain + '/user/getProfile'
        },
        COUNTRIES:{
          GET_MANY: apiRoot + '/countries/getMany'
        },
        CITIES:{
          GET_MANY: apiRoot + '/cities/getMany'
        },
        STREETS: {
          GET_MANY: apiRoot + '/streets/getMany'
        },
        CATEGORIES:{
          GET_ALL: apiRoot + '/categories/getAll'
        },
        TAGS: apiRoot + '/tags',
        REGISTER: apiRoot + '/register',
        REGISTER: {
          VALIDATE: apiRoot + '/users/validate',
          FINALIZE: apiRoot + '/users'
        },
        COMPLETE_REGISTER: apiRoot + '/completeRegister',
        LOGIN: apiRoot + '/login',
        SEND_CODE: apiRoot + '/sendCode',
        TOKEN: domain + '/oauth/v2/token'
      };
      return api;
    })())
    .constant('AUTH_EVENTS', {
      getClientCredentialsSuccess: 'auth-get-client-credentials-success',
      getClientCredentialsFailed: 'auth-get-client-credentials-failed',
      loginSuccess: 'auth-login-success',
      getUserProfileSuccess: 'get-user-profile-success',
      getUserProfileFailed: 'get-user-profile-failed',
      loginFailed: 'auth-login-failed',
      logoutSuccess: 'auth-logout-success',
      notAuthenticated: 'auth-not-authenticated',
      notAuthorized: 'auth-not-authorized'
    })
    .constant('LOCAL_STORAGE_KEYS', {
      authorization: 'authorization_data',
      userProfile: 'profile_data'
    })
    .constant('CARGO_TYPES', {
      PASSENGERS: 0,
    })
    .constant('DEFAULT_VALUES', {
      DEPARTURE_COUNTRY: {id: 1, name: "Беларусь"},
      DEPARTURE_CITY: {id: 1, area: "Минский район", region: "Минская область", name:"Минск"},
      ARRIVAL_COUNTRY: {id: 3, name: "Россия"},
      ARRIVAL_CITY: {id: 2, area: "Московский район", region: "Московская область", name: "Москва"}
    })
    .constant('CATEGORIES', [
      {id: 0, isPassenger: true, name: 'Пассажирские перевозки', img: '/img/typ1.png'},
      {id: 1, isPassenger: false, name: 'Мебель и быт. техника', img: '/img/typ2.png'},
      {id: 2, isPassenger: false, name: 'Переезд', img: '/img/typ3.png'},
      {id: 3, isPassenger: false, name: 'Строй-грузы и оборудование', img: '/img/typ4.png'},
      {id: 4, isPassenger: false, name: 'Вывоз мусора', img: '/img/typ5.png'},
      {id: 5, isPassenger: false, name: 'Транспортные средства', img: '/img/typ6.png'},
      {id: 6, isPassenger: false, name: 'Легковые грузоперевозки', img: '/img/lighcar.png'},
      {id: 7, isPassenger: false, name: 'Перевозки животных', img: '/img/paws.png'},
      {id: 8, isPassenger: false, name: 'Негабаритные грузы', img: '/img/typ9.png'},
      {id: 9, isPassenger: false, name: 'Продукты питания', img: '/img/typ10.png'},
      {id: 10, isPassenger: false, name: 'Покупки', img: '/img/cart.png'},
      {id: 11, isPassenger: false, name: 'Прочие грузы', img: '/img/typ12.png'},
    ])
    .constant('YEARS', (function() {
      var years = [];
      var currentYear = new Date().getFullYear();
      for(var i = 0; i < 101; i++){
        years.push(currentYear - i);
      }
      return years;
    })())
    .constant('MONTHS', (function() {
      var months = [{name:"Январь", number:1},
                    {name:"Февраль", number:2},
                    {name:"Март", number:3},
                    {name:"Апрель", number:4},
                    {name:"Май", number:5},
                    {name:"Июнь", number:6},
                    {name:"Июль", number:7},
                    {name:"Август", number:8},
                    {name:"Сентябрь", number:9},
                    {name:"Октябрь", number:10},
                    {name:"Ноябрь", number:11},
                    {name:"Декабрь", number:12}];
      return months;
    })())
    .constant('PHONE_CODES', (function() {
      var phoneCodes = [{name:"BLR", code:"375"},
                        {name:"RU", code:"7"}];
      return phoneCodes;
    })())
     .constant('DATE_RANGES', (function() {
      return {
        DAY: 1,
        WEEK: 7,
        MONTH: 30,
        YEAR: 365
      };
    })());
})();
