var gulp = require('gulp');
var bower = require('gulp-bower');
var mainBowerFiles = require('main-bower-files');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var jshint = require('gulp-jshint');
var minifyCSS = require('gulp-minify-css');
var del = require('del');
var livereload = require('gulp-livereload');
var gulpFilter = require('gulp-filter');
var flatten = require('gulp-flatten');
var imagemin = require('gulp-imagemin');
var inject = require('gulp-inject');
var csslint = require('gulp-csslint');
var gutil = require( 'gulp-util' );
var webserver = require('gulp-webserver');
var opn = require('opn');
var ftp = require( 'vinyl-ftp' );
var stylus = require('gulp-stylus');
var autoprefixer = require('gulp-autoprefixer');

var server = {
  host: 'localhost',
  port: '8001',
  path: 'src/'
};

//fetching of the bower components
var bowerDir = 'src/bower_components';
gulp.task('bower', function() {
    return bower()
        .pipe(gulp.dest(bowerDir))
});

gulp.task('jshint', function(){
	gulp.src(['src/app/**/*.js'])
		.pipe(jshint())
		.pipe(jshint.reporter('jshint-stylish'))
});

gulp.task('csslint', function() {
    gulp.src('src/css/*.css')
        .pipe(csslint())
        .pipe(csslint.reporter());
});

gulp.task('reload', function () {
    livereload.reload("src/index.html");
});

gulp.task('watch', function () {
	livereload.listen();
	gulp.watch(['src/app/**/*.js', 'src/stylus/**/*.styl', 'src/css/*.css', '!src/css/common.css', 'src/*.html', 'src/app/templates/**/*', 'src/app/views/**/*', 'gulpfile.js'],
        ['jshint', 'styles', 'reload']);
});

gulp.task('webserver', function() {
    gulp.src( server.path )
        .pipe(webserver({
            host: server.host,
            port: server.port,
            livereload: true,
            directoryListing: false
        }));
});

gulp.task('openbrowser', function() {
    opn( 'http://' + server.host + ':' + server.port );
});

gulp.task('default', ['webserver', 'styles', 'watch', 'openbrowser']);

// ------------------------------ BUILD ---------------------------------- //

gulp.task('clean', function() {
    del(['dist/**/*', 'dist']);
});

gulp.task('publish-bower-components', function () {
    var jsFilter = gulpFilter('*.js');
    var cssFilter = gulpFilter('*.css');

    var bowerFiles = mainBowerFiles({
        includeDev: true,
        overrides: {
            bootstrap: {
                main: [
                    './dist/js/bootstrap.js',
                    './dist/css/bootstrap.css',
                    './dist/fonts/*.*'
                ]
            },
            'angular-bootstrap': {
                main: [
                    './ui-bootstrap.js',
                    './ui-bootstrap-tpls.js'
                ]
            },
            'jquery-ui': {
                main: [
                    './themes/base/jquery-ui.min.css'
                ]
            }
        }
    });

    console.log(bowerFiles);

    // project custom js plugins
    var customJSPlugins = ['src/js/*.js'];

    // grab vendor js files from bower_components, minify and push in /dist   
    gulp.src(bowerFiles.concat(customJSPlugins))
        .pipe(jsFilter)
        .pipe(concat('libs.js'))
        .pipe(uglify())
        .pipe(gulp.dest('dist/libs/'))

    // grab vendor css files from bower_components, minify and push in /dist
    gulp.src(bowerFiles)
		.pipe(cssFilter)
		.pipe(concat('libs.css'))
		.pipe(minifyCSS())
		.pipe(gulp.dest('dist/libs/'));
});


gulp.task('publish-project-js', function(){
	gulp.src(['src/app/**/*.js'])
	    .pipe(concat('scripts.js'))
	    .pipe(uglify())
	    .pipe(gulp.dest('dist/js/'))
});

gulp.task('publish-project-css', function(){
	gulp.src(['src/css/*.css'])
	    .pipe(concat('styles.css'))
	    .pipe(minifyCSS())
	    .pipe(gulp.dest('dist/css/'))
});

gulp.task('publish-project-fonts', function(){
    gulp.src(['src/fonts/*'])
		.pipe(gulp.dest('dist/fonts/'));
});

gulp.task('publish-project-images', function() {
    gulp.src('src/img/**/*')
        .pipe(imagemin({
            progressive: true
       	}))
        .pipe(gulp.dest('dist/img'));
});

gulp.task('publish-html', function(){
    gulp.src(['src/app/views/**/*'])
		.pipe(gulp.dest('dist/app/views'));

    gulp.src(['src/app/templates/**/*'])
		.pipe(gulp.dest('dist/app/templates'));
});

gulp.task('publish-other-files', function(){
    gulp.src(['src/typings/**/*'])
		.pipe(gulp.dest('dist/typings'));
});

gulp.task('styles', function () {
    gulp.src('./src/stylus/common.styl')
        .pipe(stylus({
            compress: false
        }))
        .pipe(autoprefixer({
            browsers: ['> 1%', 'last 4 versions', 'Firefox ESR', 'Opera 12.1'],
            cascade: false
        }))
        .pipe(gulp.dest('./src/css'));
});

gulp.task('publish-index', function(){
    gulp.src('src/index.html')
        .pipe(inject(gulp.src('dist/libs/libs.css', {read: false}), {name: 'bower_css', ignorePath: 'dist/'}))
        .pipe(inject(gulp.src('dist/css/styles.css', {read: false}), {name: 'project_css', ignorePath: 'dist/'}))
        .pipe(inject(gulp.src('dist/libs/libs.js', {read: false}), {name: 'bower_js', ignorePath: 'dist/'}))
        .pipe(inject(gulp.src('dist/js/scripts.js', {read: false}), {name: 'app_js', ignorePath: 'dist/'}))
        .pipe(gulp.dest('dist'));
});

gulp.task('publish', 
	['publish-project-fonts', 'publish-bower-components', 'publish-project-js', 
	'publish-project-css', 'publish-project-images', 'publish-html', 'publish-other-files', 'publish-index']
);

gulp.task('build', ['publish-index']);

// ------------------------------ DEPLOY ---------------------------------- //

gulp.task('deploy', function (){
    var conn = ftp.create( {
        host:     '',
        user:     '',
        password: '',
        parallel: 10,
        log:      gutil.log
    } );

    var globs = [
        'dist/**'
    ];

    return gulp.src( globs, { base: './dist/', buffer: false } )
        //uncomment the line below if you want to remove any directory from FTP server before deployment
        //.pipe(conn.rmdir('/site/wwwroot/any_directory_name'))  
        .pipe( conn.newer( '/site/wwwroot' ) ) // only upload newer files
        .pipe( conn.dest( '/site/wwwroot' ) );
});